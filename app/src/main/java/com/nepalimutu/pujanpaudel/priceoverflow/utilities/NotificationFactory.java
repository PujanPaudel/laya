package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Notifications;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Date;

/**
 * Created by pujan paudel on 9/16/2015.
 */
public class NotificationFactory {
    /**
     * @Params
     * @ModelPart
     * Sender-ParseUser
     * Reciever-ParseUser
     * NotificationType-Enum or Integer
     * Poem Associated -PoemsPost Object ( Pointer )
     */
    private ParseUser sender;
    private ParseUser reciever;
    private int notificationtype;
    private PoemsPost linkedpoem;
    private Date notifdate;
    private int ratings;
    private String poemname;
    private String sendername;


    public NotificationFactory(ParseUser sender, ParseUser reciever, int notificationtype, PoemsPost linkedpoem,Date date,int rating,String pname,String sname) {
        this.sender = sender;
        this.reciever = reciever;
        this.notificationtype = notificationtype;
        this.linkedpoem = linkedpoem;
        this.notifdate=date;
        this.ratings=rating;
        this.poemname=pname;
        this.sendername=sname;
    }


    public void Parcel(){

        Notifications nf=new Notifications();
        nf.setCategory(notificationtype);
        nf.setPoem(linkedpoem);
        nf.setReciever(reciever);
        nf.setSender(sender);
        nf.setDate(notifdate);
        nf.setRating(ratings);
        nf.setPoemName(poemname);
        nf.setSenderName(sendername);
        ParseACL acl=new ParseACL();
        acl.setPublicReadAccess(true);
        acl.setPublicWriteAccess(true);
        nf.setACL(acl);
        nf.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null){
                    Toast.makeText(HomeActivity.reference.getApplicationContext(), "Notification Sent ", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(HomeActivity.reference.getApplicationContext(),"Network Failure,Try Again",Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
    }
}
