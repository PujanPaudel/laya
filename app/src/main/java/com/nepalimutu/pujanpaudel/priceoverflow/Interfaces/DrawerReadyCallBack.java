package com.nepalimutu.pujanpaudel.priceoverflow.Interfaces;

/**
 * Created by pujan paudel on 10/11/2015.
 */
public interface DrawerReadyCallBack {
    public void DrawerReady();
}
