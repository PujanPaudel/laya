package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

/**
 * Created by pujan paudel on 9/14/2015.
 */
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.FollowList;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import org.json.JSONArray;

public class SignUpFragment  extends Fragment{
    private EditText usernameEditText;
    private EditText passwordEditText;
    private EditText passwordAgainEditText;
    private Button mActionButton;

    public void onCreate(Bundle savedInstancestate){
        super.onCreate(savedInstancestate);



    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.signup, container, false);

        usernameEditText=(EditText)rootView.findViewById(R.id.username_edit_text);
        passwordEditText=(EditText)rootView.findViewById(R.id.password_edit_text);
        passwordAgainEditText=(EditText)rootView.findViewById(R.id.password_again_edit_text);
        mActionButton=(Button)rootView.findViewById(R.id.action_button);
        //Show What This basiccally is
        //When we press the Enter Button

        passwordAgainEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            //Show What This basiccally is
            //When we press the Enter Button
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // TODO Auto-generated method stub
                if(actionId==R.id.edittext_action_signup||actionId==EditorInfo.IME_ACTION_UNSPECIFIED){
                    signup();
                    return true;
                }
                return false;
            }
        });
//Set up the submit button click handler


        mActionButton.setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                signup();
            }

        });

        return rootView;
    }
    protected void signup() {
        // TODO Auto-generated method stub

        // TODO Auto-generated method stub
        String username=usernameEditText.getText().toString().trim();
        String password=passwordEditText.getText().toString().trim();
        String passwordAgain=passwordAgainEditText.getText().toString().trim();

        //Validate the sign up data
        //Client Side verification
        boolean validationError=false;
        StringBuilder validationErrorMessage=new StringBuilder("Please");
        if(username.length()==0){
            validationError=true;
            validationErrorMessage.append("Enter a username");

        }
        if(password.length()==0){
            if(validationError){
                validationErrorMessage.append("and");
            }
            validationError=true;
            validationErrorMessage.append("Enter a password");
        }
        if(!password.equals(passwordAgain)){
            if(validationError){
                validationErrorMessage.append("and");
            }
            validationError=true;
            validationErrorMessage.append("Mismatched Passwords");
        }

        validationErrorMessage.append(".");

        //If there is a validation error , display the error
        if(validationError){
            Toast.makeText(getActivity(), validationErrorMessage.toString(),Toast.LENGTH_LONG).show();
            return;
        }

        //Set up a proress dialog
        final ProgressDialog dialog=new ProgressDialog(getActivity());
        dialog.setMessage("Signung Up");
        dialog.show();


        ParseUser user=new ParseUser();
        user.setUsername(username);//That Extra Space is for Our Regex Bug :)
        user.setPassword(password);
        user.put("Rating",0); //Put the Rating Thing to Zero
        user.put("Points",0);
        user.put("Poems",0);
        //Call the parse Signup method
        user.signUpInBackground(new SignUpCallback(){

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                if(e!=null){
                    //Show the error message
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getActivity(), "You Signed Up!!", Toast.LENGTH_LONG);
                    Log.d("Success", "Signed  Up");

                    FollowList newfollow=new FollowList();
                    newfollow.setUser(ParseUser.getCurrentUser());
                    newfollow.setArray(new JSONArray());
                    ParseACL followACL=new ParseACL();
                    followACL.setPublicWriteAccess(true);
                    followACL.setPublicReadAccess(true);
                    newfollow.setACL(followACL);
                    newfollow.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Toast.makeText(getActivity().getApplicationContext(), "Got Everything Ready", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }
                    });



                }

            }

        });

    }





}
