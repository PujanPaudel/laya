package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.app.ProgressDialog;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.model.CommentsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Date;

/**
 * Created by pujan paudel on 9/16/2015.
 */
public class CommentFactory {
    private PoemsPost post;
    private ParseUser commentor;
    private Date commentdate;
    private String commentbody;
    private ProgressDialog pdialog;

    public CommentFactory(String commentbody, ParseUser commentor, Date commentdate, PoemsPost post) {
        this.commentbody = commentbody;
        this.commentor = commentor;
        this.commentdate = commentdate;
        this.post = post;
        pdialog=new ProgressDialog(HomeActivity.reference);
    }

    public  void Parcel(){
        final CommentsPost cp=new CommentsPost();
        cp.setPostObject(this.post);
        cp.setCommentBody(this.commentbody);
        cp.setCommentDate(this.commentdate);
        cp.setCommentorObject(this.commentor);

        ParseACL acl=new ParseACL();
        acl.setPublicReadAccess(true);
        acl.setPublicWriteAccess(true);
        cp.setACL(acl);
        pdialog.setMessage("Posting The Comment");
        pdialog.show();

        cp.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                pdialog.dismiss();
                if (e == null) {
                    Toast.makeText(HomeActivity.reference.getApplicationContext(), "Posted The Comment", Toast.LENGTH_LONG).show();
                    //Comment Pinned In Background
                } else {
                    Toast.makeText(HomeActivity.reference.getApplicationContext(), "Network Failure,Try Again", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });

    }
}