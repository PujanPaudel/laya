package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by pujan paudel on 10/1/2015.
 */
public class StaticConfiguration {
    private static PoemsPost mypoem;
    private static List<ParseUser> searches;
    public static void setPoemsPost(PoemsPost poem){

        mypoem=poem;
    }
    public static PoemsPost getPeom(){
        return mypoem;
    }


    public static void setSearchesList(List<ParseUser>mysearches){
        searches=mysearches;
    }
    public static List<ParseUser>getList(){
        return searches;
    }



}
