package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by pujan paudel on 9/20/2015.
 */
public class FollowCache extends SQLiteOpenHelper {

    //All Static VAriables
    //DATABASE VERSION
    private static int DATABASE_VERSION = 1;
    //Database NAme
    private static final String DATABASE_NAME = "FOLLOWERSCACHE";
    //Database Table Name
    private static final String TABLE_CACHE = "FOLLOWCACHE";

    //Different Feeds for Different maybe

    private static final String KEY_ID = "id";
    private static final String KEY_PROFILEID = "objectid";


    public FollowCache(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        // TODO Auto-generated constructor stub
    }

    //Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub


        String CREATE_FEED_TABLE = "CREATE TABLE " + TABLE_CACHE + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_PROFILEID + " TEXT"+ ")";

        Log.d("onCreate", CREATE_FEED_TABLE);
        db.execSQL(CREATE_FEED_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        Log.d("Wooh", "On Upgrade");

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CACHE);
        onCreate(db);

    }

    //NOW ALL THE CRUD OPERATIONS

    //Addding a NEw Feed
    public void addUser( String objid) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PROFILEID, objid);

        //Inserting row
        db.insert(TABLE_CACHE, null, values);
        db.close();
    }


    //For Getting Single Contact
    public boolean getUser(String id) {
//The ID is String
        SQLiteDatabase db = this.getReadableDatabase();
        //Query for getting a Single ID
        //Problem Here
        //get The reflection of full News Also !!
        Cursor cursor = db.query(TABLE_CACHE, new String[]{KEY_ID, KEY_PROFILEID}, KEY_PROFILEID + "=?", new String[]{id}, null, null, null, null);
        //THe Reflection Starts From 0

        if (cursor.getCount() == 0) {
            return false; //got , its not empty
        } else {
            return true;// not got , its empty
        }


    }


}