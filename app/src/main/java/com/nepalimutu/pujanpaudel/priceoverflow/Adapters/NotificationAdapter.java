package com.nepalimutu.pujanpaudel.priceoverflow.Adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Notifications;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Poems;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StaticEnums;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StopLoading;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.Date;
import java.util.List;

/**
 * Created by pujan paudel on 9/17/2015.
 */
public class NotificationAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StopLoading {
    private List<Notifications>notificationsList;
    private String MESSAGE_COMMENT="Commented On :";
    private String MESSAGE_RATE="Rated  ";
    private String MESSAGE_SHARE="Shared your Poem";
    private String MESSAGE_NEW_POST="Posted a New Poem";
    private String MESSAGE_ALSO_COMMENTED="Also Commented on the Poem ";
    private int mLastPosition=0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER=2;
    private static final int TYPE_HEADER=0;
    private Context ctx;
    private LoadingViewFooter footerholder;

    public NotificationAdapter(List<Notifications> notilist) {
        this.notificationsList = notilist;

    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ctx=parent.getContext();
        if(viewType==TYPE_HEADER){
            View itemView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.emptyview, parent, false);
            return new EmptyHeader(itemView);
        }else if(viewType==TYPE_FOOTER){
            Log.d("Sending","FooterType");
            View itemView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.progressloadingfooter, parent, false);
            return new LoadingViewFooter(itemView);

        }else if(viewType==TYPE_ITEM){
            View itemView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.notification_cardview, parent, false);
            return new NotificationViewHolder(itemView);
        }
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.notification_cardview, parent, false);
        return new NotificationViewHolder(itemView);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof NotificationViewHolder){
            final NotificationViewHolder myNotification=(NotificationViewHolder)holder;

            myNotification.currentItem=notificationsList.get(position-1);
            float initialTranslation=(mLastPosition<=position?1500f:-150f);
            final Notifications ci = notificationsList.get(position-1);
            //The Adpating aprt goes here
            final PoemsPost temp=(PoemsPost)ci.getPoem();

            myNotification.currentview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.reference.PoemDetail(temp,false,null);
                }
            });

            //The Comment Showing part

            if(ci.getCategory()== StaticEnums.Notification.COMMENT.ordinal()){
                // myNotification.vTitle.setText(ci.getSender().fetchIfNeeded().getString("DisplayName") +"  "+ MESSAGE_COMMENT);
                myNotification.vTitle.setText(ci.getSenderName()+ " "+ MESSAGE_COMMENT);
                myNotification.vNotificationType.setImageResource(R.drawable.new_comment);

            }else if(ci.getCategory()==StaticEnums.Notification.RATE.ordinal()){
                // myNotification.vTitle.setText(ci.getSender().fetchIfNeeded().getString("DisplayName") + "  "+MESSAGE_RATE +String.valueOf(ci.getRating())+ "  Points To ");
                myNotification.vNotificationType.setImageResource(R.drawable.action_like);
                myNotification.vTitle.setText(ci.getSenderName() + " " + MESSAGE_RATE +String.valueOf(ci.getRating())+ "  Points To " );

            }else if (ci.getCategory()==StaticEnums.Notification.FOLLOWPOST.ordinal()){

                //  myNotification.vTitle.setText(ci.getSender().fetchIfNeeded().getString("DisplayName") + " "+MESSAGE_NEW_POST);
                myNotification.vNotificationType.setImageResource(R.drawable.poemtype_poem);
                myNotification.vTitle.setText(ci.getSenderName()+" "+ MESSAGE_NEW_POST);

            } else if(ci.getCategory()==StaticEnums.Notification.SHARE.ordinal()){
                //    myNotification.vTitle.setText(ci.getSender().fetchIfNeeded().getString("DisplayName") + "  "+MESSAGE_SHARE);
                myNotification.vNotificationType.setImageResource(R.drawable.action_share);
                myNotification.vTitle.setText(ci.getSenderName()+" " +MESSAGE_SHARE);

            }else if(ci.getCategory()==StaticEnums.Notification.FOLLOWCOMMENT.ordinal()){
                // myNotification.vTitle.setText(ci.getSender().fetchIfNeeded().getString("DisplayName") + "  "+MESSAGE_ALSO_COMMENTED);
                myNotification.vNotificationType.setImageResource(R.drawable.action_comment);
                myNotification.vTitle.setText(ci.getSenderName()+ " "+MESSAGE_ALSO_COMMENTED);
            }
            myNotification.vTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.reference.OpenProfile(ci.getSender());
                }
            });

                        myNotification.vPoemTitle.setText(ci.getPoemName());


            int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME|  DateUtils.FORMAT_NO_YEAR;
            long millisecond = ci.getDate().getTime();
            String monthAndDayText = DateUtils.formatDateTime(HomeActivity.reference.getApplicationContext(),millisecond, flags);


            myNotification.vDate.setText(monthAndDayText);

            //Nothin to Do now for Noification Type
            // holder.itemView.setTranslationY(initialTranslation);
            // holder.itemView.animate().setInterpolator(new DecelerateInterpolator(1.0f)).translationY(0f).setDuration(900l).setListener(null);
            // mLastPosition=position;

        }
        else if(holder instanceof LoadingViewFooter){
            Log.d("Bind-Type","Footer");
            LoadingViewFooter footer=(LoadingViewFooter)holder;
            footerholder=footer;
            footer.progressBar.setIndeterminate(true);

        }
    }



    @Override
    public int getItemCount() {
        return notificationsList.size()+2;
    }

    @Override
    public int getItemViewType(int position){
        if(position==0){
            return TYPE_HEADER;
        }else if(position==notificationsList.size()+1){
            Log.d("getItemViewType","Footer");
            return TYPE_FOOTER;
        }else{
            return TYPE_ITEM;
        }

    }


    public static class LoadingViewFooter extends  RecyclerView.ViewHolder{
        protected ProgressBar progressBar;
        public LoadingViewFooter(View v){
            super(v);
            progressBar=(ProgressBar)v.findViewById(R.id.otherloading);
        }

    }
    public static class EmptyHeader extends RecyclerView.ViewHolder{
        public EmptyHeader(View v) {
            super(v);
        }
    }

    @Override
    public void stopLoading() {
        Log.d("Hey", "Stop Loading");
        if(footerholder!=null){
            Log.d("Yeah", "Indeterminate");
            footerholder.progressBar.setVisibility(View.GONE);
        }else{
            Log.d("Stop Shittie ","Our Tea is til not ready Then ");

        }

    }

    @Override
    public void startLoading() {
        if(footerholder!=null) {
            footerholder.progressBar.setVisibility(View.VISIBLE);
            footerholder.progressBar.setIndeterminate(true);

        }else{
            Log.d("Start Shittie ","Our Tea is til not ready Then ");

        }
    }



    public static class NotificationViewHolder extends RecyclerView.ViewHolder {
//implements View.onClickListener needed

        protected CardView cardView;
        protected TextView vTitle;
        protected TextView vPoemTitle;
        protected TextView vDate;
        protected ImageView vNotificationType;


        public Notifications currentItem;
        protected View currentview;
        public NotificationViewHolder(View v) {
            super(v);
            currentview=v;

            cardView = (CardView) v.findViewById(R.id.notification_card_view);
            vTitle =(TextView)v.findViewById(R.id.notificationtext);
            vPoemTitle=(TextView)v.findViewById(R.id.poemName);
            vDate=(TextView)v.findViewById(R.id.poemdate);
            vNotificationType=(ImageView)v.findViewById(R.id.notificationtype);



            //v.setOnClickListener(this

        }
    }
}
