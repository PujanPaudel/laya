package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.Interfaces.FollowBackCallback;
import com.nepalimutu.pujanpaudel.priceoverflow.NetworkStatus;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.FollowList;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.AvatarsURL;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.FollowCache;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.RoundedImageView;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.SnackbarShow;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.UserFollowerWrapper;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pujan paudel on 9/18/2015.
 */
public class ProfileInfoFragment extends Fragment implements FollowBackCallback{
    private TextView username,interested,bestbooks,bestauthor,bestquote,totalpoints,totalpoems;
    private ParseUser targetUser;
    private FloatingActionButton follow;
    public static ProfileInfoFragment reference;
    private boolean followbool;
    private FollowList targetfollowlist;
    private boolean personalbool;
    private LinearLayout nonetworkview;
    private RoundedImageView profile;
    private CoordinatorLayout mylayout;
    private RatingBar userrating;
    private  ScrollView contentView;
    private LinearLayout progresslayout;
    public ProfileInfoFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        reference=this;
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
    }
    @SuppressLint("ValidFragment")
    public ProfileInfoFragment(ParseUser user){
        this.targetUser=user;
    }
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.optimizedprofileinfo, container, false);
         profile=(RoundedImageView)view.findViewById(R.id.roundedprofile);
        username=(TextView)view.findViewById(R.id.username);
        interested=(TextView)view.findViewById(R.id.interestedin);
        bestauthor=(TextView)view.findViewById(R.id.bestwriter);
        bestbooks=(TextView)view.findViewById(R.id.bestbooks);
        bestquote=(TextView)view.findViewById(R.id.bestquote);
        totalpoems=(TextView)view.findViewById(R.id.totalpoems);
        totalpoints=(TextView)view.findViewById(R.id.totalpoints);
        userrating=(RatingBar)view.findViewById(R.id.userrating);
        follow=(FloatingActionButton)view.findViewById(R.id.follow);
        mylayout=(CoordinatorLayout)view.findViewById(R.id.coordinate);
       contentView=(ScrollView)view.findViewById(R.id.content_layout);
        contentView.setVisibility(View.GONE);
       progresslayout=(LinearLayout)view.findViewById(R.id.progresslayout);
        nonetworkview=(LinearLayout)view.findViewById(R.id.nonetworkScreen);
        nonetworkview.setVisibility(View.GONE);
        progresslayout.setVisibility(View.VISIBLE);
        Button refreshButton=(Button)view.findViewById(R.id.refresh);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progresslayout.setVisibility(View.VISIBLE);
                getFromNetwork();
            }
        });
        if(targetUser.getObjectId()==ParseUser.getCurrentUser().getObjectId()){
            follow.setVisibility(View.VISIBLE);
            personalbool=true;
        }

        if(!NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())){
            nonetworkview.setVisibility(View.VISIBLE);
            progresslayout.setVisibility(View.GONE);
            return view;
        }else{
            getFromNetwork();
            return view;

        }



    }

    public void getFromNetwork(){
        try {
            profile.setImageResource(AvatarsURL.URLS[Integer.parseInt(targetUser.fetchIfNeeded().getNumber("AvatarIndex").toString())]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        targetUser.fetchInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e==null){
                    ParseUser temp=(ParseUser)parseObject;
                    //progresslayout,contentView

                    progresslayout.setVisibility(View.GONE);
                    nonetworkview.setVisibility(View.GONE);
                    contentView.setVisibility(View.VISIBLE);
                   // userrating.setRating(2.25f);
                    username.setText(temp.getString("DisplayName"));
                    interested.setText(temp.getString("Interested"));
                    bestauthor.setText(temp.getString("bestauthor"));
                    bestquote.setText(temp.getString("bestquote"));

                    bestbooks.setText(temp.getString("bestbooks"));
                    float points= (int)temp.getNumber("Points");
                    float rating=points*0.0045f;
                    totalpoints.setText(String.valueOf(points));
                     userrating.setRating(rating);
                    totalpoems.setText(String.valueOf(temp.getNumber("Poems")));
                }else{
                    e.printStackTrace();
                }


            }
        });



        if(!personalbool) {
            ParseQuery<FollowList> followListParseQuery = FollowList.getQuery();
            followListParseQuery.whereEqualTo("User", targetUser);
            followListParseQuery.findInBackground(new FindCallback<FollowList>() {
                @Override
                public void done(List<FollowList> list, ParseException e) {
                    if (e == null) {
                        boolean foundflag = false;
                        FollowList myobject = list.get(0);
                        targetfollowlist = myobject;
                        JSONArray followers = myobject.getArray();
                        for (int i = 0; i < followers.length(); i++) {
                            JSONObject user = null;
                            try {
                                user = followers.getJSONObject(i);
                                if (user.getString("UserId").equals(ParseUser.getCurrentUser().getObjectId())) {
                                    Log.d("CAAUTCHAA", "Found You");
                                    foundflag = true;
                                }
                            } catch (JSONException e1) {

                                e1.printStackTrace();
                            }

                            if (foundflag) {
                                break;
                            }
                        }//For Loop Ended
                        //Do all the Puts and saves in this particular object
                        follow.setVisibility(View.VISIBLE);
                        if (foundflag) {
                            follow.setImageResource(R.drawable.deletestuff);
                            followbool = false;//iN otehr sense , do it as unfollow
                        } else {
                            follow.setImageResource(R.drawable.user_follow);
                            followbool = true;
                        }
                    }
                }
            });
        }else{
            follow.setImageResource(R.drawable.editprofile);
        }
        follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Already
                if (personalbool) {
                    Fragment fragment = new EditInfoFragment();// Just  For testing for Thyselves
                    FragmentManager manager = getActivity().getSupportFragmentManager();

                    manager.beginTransaction().replace(R.id.container, fragment)
                            .addToBackStack(null).commit();
                    return;
                }
                if (followbool) {
                    new UserFollowerWrapper(ParseUser.getCurrentUser(), targetUser, mylayout).Follow();
                } else {
                    unFollow(targetUser);

                }
            }

        });
        userrating.setFocusable(false);





    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        //StaticConfiguration.setSearchesList(searchlists);
    }

    public void unFollow(ParseUser targetUser){
                    JSONArray temp=new JSONArray();
                    JSONArray followers=targetfollowlist.getArray();
                    for(int i=0;i<followers.length();i++){
                        try {
                            JSONObject user=followers.getJSONObject(i);
                            if(user.getString("UserId").equals(ParseUser.getCurrentUser().getObjectId())){
                                Log.d("CAAUTCHAA","Found You");
                            }else {
                                temp.put(user);
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                    }
                    Log.d("Half Done ","Now Save the Modiied Object");

                    targetfollowlist.put("Followers", temp);
                    targetfollowlist.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e==null){
                                Toast.makeText(getActivity(),"Unfolloweed the User",Toast.LENGTH_LONG).show();
                                //Do the Follow Call back  to Onself hahahaha
                                follow.setImageResource(R.drawable.user_follow);
                                followbool=true;
                            }else{
                                Toast.makeText(getActivity(),"Some Network Problem try Again",Toast.LENGTH_LONG).show();

                            }
                        }
                    });
        //Remove Oneselfff

    }


    @Override
    public void HideFollow() {
follow.setImageResource(R.drawable.deletestuff);//subscribe is the placeholder now for Follow
        followbool=false;
    }
}

