package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.NetworkStatus;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Reports;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.SnackbarShow;
import com.parse.DeleteCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pujan paudel on 10/18/2015.
 */
public class ReportPost extends DialogFragment {

private PoemsPost targetPoem;
    private String[]reporttype;
    private String type;
    private ReportPost reference;
    private View snackview;
    public  ReportPost(){}

    @SuppressLint("ValidFragment")
    public ReportPost(PoemsPost tgPoem,View view){
        this.targetPoem=tgPoem;
        this.snackview=view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        reporttype=new String[]{"Plagiarism","Offesive Post","Violence","Out of Topic"};
    reference=this;
        }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.report_post, container,
                false);

        getDialog().setTitle("Report the Post");
        final Spinner mytype=(Spinner)rootView.findViewById(R.id.report_type);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, reporttype);
        mytype.setAdapter(adapter);
        mytype.setOnItemSelectedListener(new CustomOnItemSelectedListener());

        final EditText additional=(EditText)rootView.findViewById(R.id.edt_reportbody);

        final Button report=(Button)rootView.findViewById(R.id.sendReport);
        Button cancel=(Button)rootView.findViewById(R.id.cancelReport);
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Do the Task

                Reports reports=new Reports();
                reports.setExtraText(additional.getText().toString());
                reports.setReportType(type);
                reports.setReporter(ParseUser.getCurrentUser());
                reports.setReport(targetPoem);
                ParseACL acl=new ParseACL();
                acl.setPublicReadAccess(true);
                acl.setPublicWriteAccess(true);
                reports.setACL(acl);
                reports.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e==null){
                            SnackbarShow.MySnackbarShow("Thanks For Reporting!! We'll Review it as Soon as Possible",snackview);
                            List<PoemsPost>mlist=new ArrayList<PoemsPost>();
                            mlist.add(targetPoem);
                            reference.dismiss();
                        }else{
                            SnackbarShow.MySnackbarShow("Oops!! Some Internet Problem",snackview);
                            reference.dismiss();
                        }
                    }
                });



            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reference.dismiss();
            }
        });

        return rootView;
    }



    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            type=parent.getItemAtPosition(pos).toString();
            // NOw We need to Do Fucking Awesome Things HEre
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }
    }
}
