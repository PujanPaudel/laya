package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.nepalimutu.pujanpaudel.priceoverflow.R;

/**
 * Created by pujan paudel on 9/22/2015.
 */
public class SettingsActivity extends PreferenceActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.settings);

    }
}
