package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.app.ProgressDialog;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;

import java.util.Date;

/**
 * Created by pujan paudel on 9/14/2015.
 */
public class PoemsPostFactory {
    private ParseUser writerUser;
    private String writer;
    private String title;
    private String category;
    private ProgressDialog saving;
    private int points;
    private JSONArray followlist;
    private PoemsPost newPost;



    public PoemsPostFactory(ParseUser writerUser, String writer, String title, String category, String fullpoem, Date posteddate,int points) {
        super();
        saving=new ProgressDialog(HomeActivity.reference);
        this.writerUser = writerUser;
        this.writer = writer;
        this.title = title;
        this.category = category;
        this.fullpoem = fullpoem;
        this.posteddate = posteddate;
        this.points=points;
    }

    private String fullpoem;
    private Date posteddate;

    public  PoemsPost getPost(){
        return this.newPost;
    }
public void Parcel(){
    PoemsPost newPost=new PoemsPost();
    newPost.setCategory(this.category);
    newPost.setDate(this.posteddate);
    newPost.setFullPoem(this.fullpoem);
    newPost.setTitle(this.title);
    newPost.setWriter(this.writer);
    newPost.setWriterUser(this.writerUser);
    newPost.setPoints(this.points);
    newPost.setFollowersArray();
    ParseACL acl=new ParseACL();
    acl.setPublicReadAccess(true);
    acl.setPublicWriteAccess(true);
    newPost.setACL(acl);
    saving.setMessage("Posting The Poem");
    saving.show();

    
    newPost.saveInBackground(new SaveCallback() {
        @Override
        public void done(ParseException e) {
            saving.dismiss();
            if(e==null){
                Toast.makeText(HomeActivity.reference.getApplicationContext(),"Posted The Poem", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(HomeActivity.reference.getApplicationContext(),"Network Failure,Try Again",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    });
}

}
