package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.SlidingTabLayout;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pujan paudel on 10/13/2015.
 */
public class FeedsContainer extends Fragment
{
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private PagerSlidingTabStrip strip;
    private FragmentManager mRetainChildFragmentManager;
    public static FeedsContainer myreference;
    @Override
    public void onCreate(Bundle savedState){
        super.onCreate(savedState);
        Log.d("FeedsContainer", "On Create");
        setRetainInstance(true);
        myreference=this;

    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        if(mRetainChildFragmentManager!=null){
            //restore the last retained Child fragment manager to the new created fragment
            try{
                Field childFMFIeld=Fragment.class.getDeclaredField("mChildFragmentManager");
                childFMFIeld.setAccessible(true);
                childFMFIeld.set(this,mRetainChildFragmentManager);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }catch (IllegalAccessException e){
                e.printStackTrace();
            }
        }else{
            mRetainChildFragmentManager=getChildFragmentManager();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("FeedsContainer", "On Create View");
        View view = inflater.inflate(R.layout.feeds_container, container, false);
        strip=(PagerSlidingTabStrip)view.findViewById(R.id.strip);

        viewPager = (ViewPager)view.findViewById(R.id.tabanim_viewpager);
        setupViewPager(viewPager);



        //tabLayout = (TabLayout)view.findViewById(R.id.tabanim_tabs);
        //tabLayout.setupWithViewPager(viewPager);


      //  tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
        //    @Override
         //   public void onTabSelected(TabLayout.Tab tab) {
          //      viewPager.setCurrentItem(tab.getPosition());
           //     switch (tab.getPosition()) {
           //     }
          ///  }

            //@Override
            //public void onTabUnselected(TabLayout.Tab tab) {
           // }

            //@Override
            //public void onTabReselected(TabLayout.Tab tab) {
           // }
        //});


        return view;
    }
    private void setupViewPager(final ViewPager viewPager) {
        Log.d("Inside","SetUpViewPager");
        final ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        //this.categories = new String[] {
      //  "Poem", "Haiku", "Article", "Gajals", "Miscellaneous"
   // };
        adapter.addFrag(new FeedFragment("NoCategory"),"All "); //This Very Thing is for the User Variability
        adapter.addFrag(new FeedFragment("Poem"), "Poems");//This Very tHing is variable
        adapter.addFrag(new FeedFragment("Haiku"), "Haiku");
        adapter.addFrag(new FeedFragment("Article"), "Articles");
        adapter.addFrag(new FeedFragment("Gajals"), "Gajals");
        adapter.addFrag(new FeedFragment("Miscellaneous"), "Miscellaneous");

        viewPager.setAdapter(adapter);

        strip.setShouldExpand(true);
        strip.setViewPager(viewPager);


        viewPager.setOffscreenPageLimit(6);


        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                switch(position)
                {

                }
            }
        };
        strip.setOnPageChangeListener(ViewPagerListener);
    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }
        @Override
        public int getCount() {
            return mFragmentList.size();
        }
        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    public void showToast(String message){
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

//    @Override
   // public void onSaveInstanceState(Bundle savedInstanceState){
    //    super.onSaveInstanceState(savedInstanceState);
   // }

}
