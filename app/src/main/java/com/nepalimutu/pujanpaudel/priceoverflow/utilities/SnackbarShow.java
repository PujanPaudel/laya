package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by pujan paudel on 9/17/2015.
 */
public  class SnackbarShow {

    public static void MySnackbarShow(String message, View view){
        Snackbar.make(view,message,Snackbar.LENGTH_SHORT).show();
    }
}
