package com.nepalimutu.pujanpaudel.priceoverflow.utilities;
 import java.util.Iterator;
 import org.json.JSONException;
 import org.json.JSONObject;
        import android.app.Notification;
        import android.app.NotificationManager;
        import android.app.PendingIntent;
        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
 import android.support.v4.app.TaskStackBuilder;
 import android.support.v7.app.NotificationCompat;
        import android.util.Log;

        import com.nepalimutu.pujanpaudel.priceoverflow.Application;
 import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
 import com.nepalimutu.pujanpaudel.priceoverflow.R;


public class PushBroadCastReciever extends BroadcastReceiver{

    public static final String ACTION="com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity";//WHERE TO SEND
    public static final String PARSE_EXTRA_DATA_KEY="com.parse.Data";
    public static final String PARSE_JSON_ALERT_KEY="alert";
    public static final String PARSE_JSON_CHANNELS_KEY="com.parse.Channel";
    public static final String PUSH_COMMENT="Comment";

    private static final String FOLLOW_COMMENTED="Follow Commented";
    private static final String RATED="Rated";
    private static final String SHARED="Shared";
    private static final String FOLLOW_STATUS_UPDATED="Follow Status Update";

    private static final String TAG="PushBroadCastReciever";


    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        try
        {
            String action = intent.getAction();

            //"com.parse.Channel"
            String channel =
                    intent.getExtras()
                            .getString(PARSE_JSON_CHANNELS_KEY);

            JSONObject json =
                    new JSONObject(
                            intent.getExtras()
                                    .getString(PARSE_EXTRA_DATA_KEY));

            Log.d(TAG, "got action " + action + " on channel " + channel + " with:");
            Iterator itr = json.keys();
            while (itr.hasNext())
            {
                String key = (String) itr.next();
                Log.d(TAG, "..." + key + " => " + json.getString(key));
            }
            notify(context,intent,json);
        }
        catch (JSONException e)
        {
            Log.d(TAG, "JSONException: " + e.getMessage());
        }
    }
    private void notify(Context ctx, Intent i, JSONObject dataObject)
            throws JSONException
    {
        NotificationManager nm = (NotificationManager)
                ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        int icon = R.drawable.subscribe;

        Log.d("Custom Data is",dataObject.getString("customdata"));
        String tickerText="Custom Notification";
        switch (dataObject.getString("customdata")){
            case RATED:
                tickerText="A Laya User Rated Your Poem ";
                break;

            case SHARED:
            tickerText="A Laya User Shared Your Poem: ";
                break;
            case FOLLOW_COMMENTED:
            tickerText="A Laya User Commented on the Poem You are Following";
                break;

            case FOLLOW_STATUS_UPDATED:
             tickerText="Poem Updated by the Person You are Following";
                break;
            default:
                tickerText="Error in PArsing the Notification Type";

    }

        long when = System.currentTimeMillis();
        Notification n = new Notification(icon, tickerText, when);

        //Let the intent invoke the respond activit

       // if(dataObject.getString("type").equals("Notification")){
        //    Application.increaseNotificationCount();

//        }else if(dataObject.getString("type").equals("Transaction")){
  //          Application.increaseTransactionCount();
        //     }
        Application.IncreaseNotifications(ctx);

        if(Application.showNotifications(ctx)) {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx);
            mBuilder.setSmallIcon(R.drawable.subscribe);
            mBuilder.setContentTitle("A New Laya Notification");
            mBuilder.setContentText(tickerText);
            mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(ctx);
            stackBuilder.addParentStack(HomeActivity.class);

            Intent intent = new Intent(ctx, HomeActivity.class);
            //Load it with parse data
            intent.putExtra("notificationtype", PUSH_COMMENT);

            stackBuilder.addNextIntent(intent);

            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            //PendingIntent pi = PendingIntent.getActivity(ctx, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);


            nm.notify(1, mBuilder.build());
        }else{
            //Do Nothing !!
        }

    }


}


