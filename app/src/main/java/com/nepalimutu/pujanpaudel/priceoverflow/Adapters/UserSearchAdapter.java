package com.nepalimutu.pujanpaudel.priceoverflow.Adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.Image;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.AvatarsURL;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.FollowCache;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.UserFollowedCallback;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.UserFollowerWrapper;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pujan paudel on 9/22/2015.
 */
public class UserSearchAdapter extends RecyclerView.Adapter<UserSearchAdapter.UserSearchViewHolder>{
    private List<ParseUser> usersList;
    private Context ctx;
    private int mLastPosition=0;
    private View sendsnack;
    private UserSearchViewHolder currentView;


    public UserSearchAdapter(List<ParseUser>myList,View snack){
        this.usersList=myList;
        this.sendsnack=snack;
    }

    @Override
    public UserSearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ctx=parent.getContext();
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.usersearchcardview, parent, false);
        return new UserSearchViewHolder(itemView);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public void onBindViewHolder(final UserSearchViewHolder holder, int position) {
        holder.currentUser=usersList.get(position);
        float initialTranslation=(mLastPosition<=position?1500f:-150f);

        Map<String,String> params=new HashMap<String,String>();
        try {
            params.put("userid",holder.currentUser.fetchIfNeeded().getObjectId());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(holder.currentUser.getObjectId()==ParseUser.getCurrentUser().getObjectId()) {
            holder.vFollow.setVisibility(View.INVISIBLE);
        }else {
            ParseCloud.callFunctionInBackground("getFollowersList", params, new FunctionCallback<ArrayList<String>>() {
                @Override
                public void done(ArrayList<String> s, ParseException e) {
                    List<String> tokeep = new ArrayList<String>();
                    JSONArray objectsArray = new JSONArray(s);
                    for (int i = 0; i < objectsArray.length(); i++) {
                        try {
                            Gson gson = new Gson();
                            String json = gson.toJson(objectsArray.get(i));
                            JSONObject finaly = new JSONObject(json);
                            tokeep.add(finaly.getString("UserId"));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }//for loop end
                    if (tokeep.contains(ParseUser.getCurrentUser().getObjectId())) {
                        holder.vFollow.setVisibility(View.GONE);

                    } else {
                        holder.vFollow.setVisibility(View.VISIBLE);
                    }

                }
            });


        }
        holder.currentview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.reference.OpenProfile(holder.currentUser);

                }
            });


        try {
            holder.avatarView.setImageResource(AvatarsURL.URLS[Integer.parseInt(holder.currentUser.fetchIfNeeded().getNumber("AvatarIndex").toString())]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.vUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.reference.OpenProfile(holder.currentUser);

            }
        });
        holder.vFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),"Followed The User",Toast.LENGTH_LONG).show();
                new UserFollowerWrapper(ParseUser.getCurrentUser(), holder.currentUser, sendsnack).Follow();
                holder.vFollow.setVisibility(View.GONE);
            }
        });
        try {
            holder.vUserrating.setFocusable(false);
            holder.vUserName.setText(holder.currentUser.fetchIfNeeded().getString("DisplayName"));
            float points= (int) holder.currentUser.fetchIfNeeded().getNumber("Points");
            holder.vPoemlevel.setText(String.valueOf((int) points + "  Points "));
            float rating=points*0.0045f;
            holder.vUserpoints.setText(String.valueOf(holder.currentUser.fetchIfNeeded().getNumber("Poems")) + "  Poems ");
            holder.vUserrating.setRating(rating);

            Log.d("Rating", String.valueOf(String.valueOf(holder.vUserrating.getRating())));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.itemView.setTranslationY(initialTranslation);
        holder.itemView.animate().setInterpolator(new DecelerateInterpolator(1.0f)).translationY(0f).setDuration(900l).setListener(null);
        mLastPosition=position;
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public static class UserSearchViewHolder extends RecyclerView.ViewHolder implements UserFollowedCallback{

        protected CardView cardView;
        protected ImageView avatarView;
        protected TextView vUserName;
        protected TextView vUserpoints;
        protected TextView  vPoemlevel;
        protected ImageButton vFollow;
        protected RatingBar vUserrating;

        public ParseUser currentUser;
        protected View currentview;
        public UserSearchViewHolder(View v) {
            super(v);
            currentview=v;

            cardView = (CardView) v.findViewById(R.id.card_view);
            avatarView =(ImageView)v.findViewById(R.id.imgAvatar);
            vUserName =(TextView)v.findViewById(R.id.usernametext);
            vUserpoints=(TextView)v.findViewById(R.id.userpoints);
            vPoemlevel=(TextView)v.findViewById(R.id.poemlevel);
            vFollow=(ImageButton)v.findViewById(R.id.follow);
            vUserrating=(RatingBar)v.findViewById(R.id.userrating);
        }

        @Override
        public void HideFollowButton() {

        }
    }
}
