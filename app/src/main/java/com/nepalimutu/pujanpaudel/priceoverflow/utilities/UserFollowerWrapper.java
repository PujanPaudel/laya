package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.ProfileInfoFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.model.FollowList;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by pujan paudel on 9/19/2015.
 */
public class UserFollowerWrapper {
    private ParseUser newfollower;
    private ParseUser sourceuser;
    private View snackbar;
    public UserFollowerWrapper(ParseUser newfollower,ParseUser source,View view){
        this.newfollower=newfollower;
        this.sourceuser=source;
        this.snackbar=view;
    }
    public void Follow(){


            ParseQuery<FollowList> followList=FollowList.getQuery();
            followList.whereEqualTo("User", sourceuser);
            followList.findInBackground(new FindCallback<FollowList>() {
                @Override
                public void done(List<FollowList> list, ParseException e) {
                    //Just 1 Item fr 1 User
                    if (e == null) {
                        if (list.size() > 0) {
                            FollowList action = list.get(0);
                            final JSONArray present=action.getJSONArray("Followers");
                            JSONObject myobject=new JSONObject();
                            try {
                                myobject.put("UserId",newfollower.getObjectId());
                                myobject.put("UserName",newfollower.getUsername());
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }

                            present.put(myobject);
                            action.put("Followers",present);
                            action.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if(e==null){
                                        FollowCache follow=new FollowCache(HomeActivity.reference.getApplicationContext());
                                        follow.addUser(sourceuser.getObjectId());
                                    SnackbarShow.MySnackbarShow("Follwed The User", snackbar);
                                        ProfileInfoFragment.reference.HideFollow();
                                    }
                                }
                            });
                        }
                    }

                }
            });


    }

}
