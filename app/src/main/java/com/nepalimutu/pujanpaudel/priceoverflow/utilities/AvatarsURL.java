package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import com.nepalimutu.pujanpaudel.priceoverflow.R;

/**
 * Created by pujan paudel on 9/24/2015.
 */
public class AvatarsURL {
    public static int[] URLS={R.drawable.lfirst,
            R.drawable.lsecond,
            R.drawable.lthird,
            R.drawable.lfourth,
            R.drawable.lfifth,
            R.drawable.lsixth,
            R.drawable.lseventh,
            R.drawable.leight,
            R.drawable.lninth,
            R.drawable.ltenth,
            R.drawable.leleventh,
            R.drawable.ltwelveth,
            R.drawable.lthirteen,
            R.drawable.lfourteen,
            R.drawable.lfifteen,
            R.drawable.lsixteen
     };

}
