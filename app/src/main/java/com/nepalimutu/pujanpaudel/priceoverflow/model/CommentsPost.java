package com.nepalimutu.pujanpaudel.priceoverflow.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Date;

/**
 * Created by pujan paudel on 9/16/2015.
 */
@ParseClassName("Comments")
public class CommentsPost extends ParseObject {
    public CommentsPost(){
        super();
    }

    public void setPostObject(PoemsPost poem){
        put("Post", poem);
    }
    public ParseObject getPostObject(){
        return getParseObject("Post");
    }

    public ParseUser getCommentorObject(){
        return getParseUser("CommentorUser");
    }
    public void setCommentorObject(ParseUser user){
        put("CommentorUser",user);
    }

    public Date getCommentDate(){
        return getDate("CommentDate");
    }
    public void setCommentDate(Date cmt){
        put("CommentDate",cmt);
    }

    public String getCommentBody(){
        return getString("CommentBody");
    }
    public void  setCommentBody(String cbody){
        put("CommentBody",cbody);
    }


    public static ParseQuery<CommentsPost> getQuery(){
        return ParseQuery.getQuery(CommentsPost.class);
    }
}
