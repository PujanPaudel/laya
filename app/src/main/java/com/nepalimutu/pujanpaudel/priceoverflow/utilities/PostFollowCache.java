package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by pujan paudel on 9/20/2015.
 */
public class PostFollowCache extends SQLiteOpenHelper {

    //All Static VAriables
    //DATABASE VERSION
    private static int DATABASE_VERSION = 1;
    //Database NAme
    private static final String DATABASE_NAME = "POSTS_CACHE";
    //Database Table Name
    private static final String TABLE_CACHE = "LIKES_CACHE";

    //Different Feeds for Different maybe

    private static final String KEY_ID = "id";
    private static final String KEY_POSTID = "objectid";


    public PostFollowCache(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        // TODO Auto-generated constructor stub
    }

    //Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub


        String CREATE_FEED_TABLE = "CREATE TABLE " + TABLE_CACHE + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_POSTID + " TEXT" + ")";

        Log.d("onCreate", CREATE_FEED_TABLE);
        db.execSQL(CREATE_FEED_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        Log.d("Wooh", "On Upgrade");

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CACHE);
        onCreate(db);

    }

    //NOW ALL THE CRUD OPERATIONS

    //Addding a NEw Feed
    public void addPostFollow(String objid) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_POSTID, objid);

        //Inserting row
        db.insert(TABLE_CACHE, null, values);
        db.close();
    }


    //For Getting Single Contact
    public boolean getPostCache(String id) {
//The ID is String
        SQLiteDatabase db = this.getReadableDatabase();
        //Query for getting a Single ID
        //Problem Here
        //get The reflection of full News Also !!
        Cursor cursor = db.query(TABLE_CACHE, new String[]{KEY_ID, KEY_POSTID}, KEY_POSTID + "=?", new String[]{id}, null, null, null, null);
        //THe Reflection Starts From 0

        if (cursor.getCount() == 0) {
            return false; //got , its not empty
        } else {
            return true;// not got , its empty
        }


    }


}