package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.Image;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AlertDialog;

import com.nepalimutu.pujanpaudel.priceoverflow.Adapters.CommentAdapter;
import com.nepalimutu.pujanpaudel.priceoverflow.Adapters.PostAdapter;
import com.nepalimutu.pujanpaudel.priceoverflow.Application;
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.Interfaces.CommentDeletedCallBack;
import com.nepalimutu.pujanpaudel.priceoverflow.NetworkStatus;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Comments;
import com.nepalimutu.pujanpaudel.priceoverflow.model.CommentsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.ContactInfo;
import com.nepalimutu.pujanpaudel.priceoverflow.model.FollowList;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Poems;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Post;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.CommentFactory;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.DeleteObject;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.EndlessRecyclerOnScrollListener;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.FavouriteDataBaseHandler;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.NewCommentPopUp;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.NotificationFactory;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.ParseProxyObject;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.PostFollowCache;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.PostFollowWrapper;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.RatingsDisplay;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.RedirectToRegister;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.ShareCallback;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.SnackbarShow;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StaticConfiguration;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StaticEnums;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pujan paudel on 8/25/2015.
 */
public class CommentFragment extends Fragment implements NewCommentPopUp,RatingsDisplay,ShareCallback,CommentDeletedCallBack{
    private ProgressDialog pdial;
    public static CommentFragment reference;
    private RecyclerView recList;
    private CardView postCard;
    private TextView poembody;
    List<Comments> types = new ArrayList<Comments>();
    private ImageButton favourite;
    private static PoemsPost currentpoem;
    private List<CommentsPost> commentsfeed=new ArrayList<CommentsPost>();
    public CommentAdapter ca;
    private View snackview;
    private int currentskip=0;
    private boolean delete=false;
    LinearLayoutManager llm;
    private boolean shareflag;
    private PoemsPost sharedpoem;
    private boolean nodatacache=false;
    public static String CommentTag="Comments";
    int lastitemindex;
    private boolean loading=true;
    private List<CommentsPost>localposts;
    private RelativeLayout detail;
    private LinearLayout nodata;
    private TextView poemtitle,poemwriter,poempoints,poemdate;
    private ImageButton report;
    private CoordinatorLayout myview;
    public CommentFragment(){
        Log.d("Constructor","Empty Constructor");
        this.currentpoem=StaticConfiguration.getPeom();
    }
    @SuppressLint("ValidFragment")
    public  CommentFragment(PoemsPost mpoem,boolean sflag,PoemsPost sPoem){
        Log.d("Constructor","Parameterized Constructor");
        this.currentpoem=mpoem;
        this.shareflag=sflag;
        this.sharedpoem=sPoem;
    }
    @Override
    public void onCreate(Bundle savedInstanceState){
        Log.d("Comment Fragment","OnCreate");
        reference=this;
       super.onCreate(savedInstanceState);
        //   window.setGravity(Gravity.BOTTOM);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.comments_feed, container, false);
         myview=(CoordinatorLayout)view.findViewById(R.id.commentlayout);
        recList = (RecyclerView)view.findViewById(R.id.commentsList);
        favourite=(ImageButton)view.findViewById(R.id.favourite);
        snackview=(CoordinatorLayout)view.findViewById(R.id.commentlayout);
        favourite.setImageResource(R.drawable.bookmark_small);

      poemtitle = (TextView) view.findViewById(R.id.title);
       poemwriter = (TextView) view.findViewById(R.id.poemwriter);
        poempoints = (TextView) view.findViewById(R.id.poempoints);
         poemdate = (TextView) view.findViewById(R.id.date);

        report=(ImageButton)view.findViewById(R.id.reportsmiley);
        detail=(RelativeLayout)view.findViewById(R.id.poemdetai);
        nodata=(LinearLayout)view.findViewById(R.id.nodataScreen);
        nodata.setVisibility(View.GONE);

        recList.setHasFixedSize(false);
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        ca = new CommentAdapter(commentsfeed, this.currentpoem, myview);
        recList.setAdapter(ca);
        ParseQuery<PoemsPost>check=PoemsPost.getQuery();
        check.whereEqualTo("objectId", currentpoem.getObjectId());
        if(!NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())){
            check.fromLocalDatastore();
        }
        check.getFirstInBackground(new GetCallback<PoemsPost>() {
            public void done(PoemsPost poemsPost, ParseException e) {
                if (e == null) {
                    //object exists            at com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.CommentFragment.onCreateView(CommentFragment.java:156)

                    DataFilling();
                } else {
                    if (e.getCode() == ParseException.OBJECT_NOT_FOUND) {
                        nodata.setVisibility(View.VISIBLE);
                        detail.setVisibility(View.GONE);
                        recList.setVisibility(View.GONE);
                        }
                }
            }
        });





    return view;

    }


    public void DataFilling(){

        if (!shareflag) {
            if(NetworkStatus.isNetworkConnected(getActivity().getApplicationContext())) {
                try {

                    if (currentpoem.getWriterUser().fetchIfNeeded().getObjectId() == ParseUser.getCurrentUser().getObjectId()) {
                        favourite.setImageResource(R.drawable.action_delete);
                        delete = true;
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }else{
                favourite.setImageResource(R.drawable.bookmark_small);  //Why the Fuck Do We need the Dlete opton
                //And no need for the delete boolean too
            }
        } else {
            //ShareFlag
            if(NetworkStatus.isNetworkConnected(getActivity().getApplicationContext())) {


                try {
                    if (sharedpoem.getThirdParty().fetchIfNeeded().getObjectId() == ParseUser.getCurrentUser().getObjectId()) {
                        favourite.setImageResource(R.drawable.action_delete);
                        delete = true;

                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }else {
                favourite.setImageResource(R.drawable.bookmark_small);
                //And no need for the delete boolean too

            }
        }
        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Do The Actual Job of Favouriting Now
                if (!delete) {
                    FavouriteDataBaseHandler favouriteDataBaseHandler = new FavouriteDataBaseHandler(HomeActivity.reference.getApplicationContext());
                    int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME|  DateUtils.FORMAT_NO_YEAR;
                    long millisecond =currentpoem.getDate().getTime();
                    String monthAndDayText = DateUtils.formatDateTime(HomeActivity.reference.getApplicationContext(),millisecond, flags);
                    favouriteDataBaseHandler.addPoem(new Poems(currentpoem.getTitle(), currentpoem.getWriter(), currentpoem.getFullPoem(), String.valueOf(-1),monthAndDayText, currentpoem.getCategory(), -1, currentpoem.getObjectId()));
                } else {
                    //Do The Deleteing Task
                    ShowAlertDialog();
                }

            }
        });




        poemtitle.setText(this.currentpoem.getTitle());
        poemwriter.setText(this.currentpoem.getWriter());
        poempoints.setText(String.valueOf(this.currentpoem.getPoints()) + "  " + "Points");
        int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_NO_YEAR;
        long millisecond = this.currentpoem.getDate().getTime();
        String monthAndDayText = DateUtils.formatDateTime(HomeActivity.reference.getApplicationContext(), millisecond, flags);


        poemdate.setText(monthAndDayText);

        //  R.id.expand_text_view
        //poembody=(TextView)view.findViewById(R.id.poembody);

        poemwriter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NetworkStatus.isNetworkConnected(getActivity().getApplicationContext())){
                    HomeActivity.reference.OpenProfile(currentpoem.getWriterUser());
                }else{
                    SnackbarShow.MySnackbarShow("Can't View User Profile While OFfline",v);
                }
            }
        });
        ParseQuery<CommentsPost> poemsquery = CommentsPost.getQuery();
        poemsquery.orderByDescending("createdAt");

        poemsquery.fromPin(CommentTag);
        poemsquery.include("user");
        poemsquery.whereEqualTo("Post", currentpoem);
        poemsquery.setLimit(10);//Limit=3
        //But in Acn
        poemsquery.findInBackground(new FindCallback<CommentsPost>() {
            @Override
            public void done(List<CommentsPost> list, ParseException e) {

                if (e != null) {
                    Toast.makeText(getActivity(), "Some Network Problem", Toast.LENGTH_LONG).show();
                } else {
                    Log.d("Items", String.valueOf(list.size()));
                    for (CommentsPost poems : list) {
                        commentsfeed.add(poems);
                    }
                    localposts = new ArrayList<CommentsPost>(commentsfeed);
                    ca.notifyDataSetChanged();
                    recList.scrollBy(1, 0);
                    if(NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())){
                        retreiveList(true);
                    }else{
                        if(list.size()==0){
                        }
                    }
                }

            }
        });


        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())){
                    SnackbarShow.MySnackbarShow("Can't Report while Offline", snackview);
                    return;
                }
                ReportPost report=new ReportPost(currentpoem,snackview);
                report.show(getActivity().getSupportFragmentManager(), "Report the Poem ");
            }
        });
    }

    public void retreiveList(final boolean clearflag){
        // pdial.show();
        //The Clear too, is Conditional My Friend
        ParseQuery<CommentsPost> poemsquery = CommentsPost.getQuery();
        poemsquery.orderByDescending("createdAt");
        ca.startLoading();
        poemsquery.include("user");
        poemsquery.whereEqualTo("Post", currentpoem);
        poemsquery.setSkip(currentskip);
        poemsquery.setLimit(5);
        poemsquery.findInBackground(new FindCallback<CommentsPost>() {
            @Override
            public void done(List<CommentsPost> list, ParseException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "Some Network Problem", Toast.LENGTH_LONG).show();
                } else {
                    if (clearflag) {
                        commentsfeed.clear();
                        lastitemindex = 0;
                    } else {
                        lastitemindex = commentsfeed.size();
                    }
                    Log.d("Items", String.valueOf(list.size()));
                    for (CommentsPost poems : list) {
                        commentsfeed.add(poems);
                    }

                    if (list.size() == 0) {
                        ca.contentflag = true;
                    } else {
                        currentskip += 5;
                        ca.contentflag = false;
                    }
//-1 or +1
                    ca.notifyDataSetChanged();
                    ca.stopLoading();
                    cleanAndAdd(localposts, list, clearflag);

                    recList.scrollBy(1, 0);
                    recList.addOnScrollListener(new EndlessRecyclerOnScrollListener(llm) {
                        @Override
                        public void onLoadMore() {
                            retreiveList(false);
                        }
                    });

                }
            }
        });
        return;
    }

    public void ScrollBottom(){
        recList.scrollBy(0, 10000);
    }
    public void AddComment(){
        Toast.makeText(getActivity(), "Adding Comment Now", Toast.LENGTH_LONG).show();

        final AlertDialog builder = new AlertDialog.Builder(getActivity(), R.style.AddComment)
                .setPositiveButton("Comment", null)
                .setNegativeButton("Cancel", null)
                .create();

        LinearLayout.LayoutParams edittextLayoutPArams = new  LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        edittextLayoutPArams.setMargins(24, 24, 24, 0);//l,t,r,b
        final EditText etComment = new EditText(getActivity());
        etComment.setEms(10);
        etComment.setSingleLine(false);
        etComment.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        etComment.setHint("Comment");
        etComment.setTextAppearance(getActivity(), R.style.TextAppearance_AppCompat_Medium);
        etComment.setLayoutParams(edittextLayoutPArams);
        builder.setView(etComment);

        builder.setTitle("Post Comment");

        builder.setMessage("Comment On The Poem");

        builder.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                final Button btnAccept = builder.getButton(AlertDialog.BUTTON_POSITIVE);
                btnAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (etComment.getText().toString().isEmpty()) {
                            Toast.makeText(getActivity(), "Don't Enter Empty Comment", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d("COmment Fragment", "You have entered: " + etComment.getText().toString());
                            builder.dismiss();
                            PostComment(etComment.getText().toString(), currentpoem);
                        }
                    }
                });

                final Button btnDecline = builder.getButton(DialogInterface.BUTTON_NEGATIVE);
                btnDecline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("Comment Fragment", "Comment declined");
                        builder.dismiss();
                    }
                });
            }
        });

        /* Show the dialog */
        builder.show();
    }

    public void PostComment(String comment,PoemsPost poem){
        CommentFactory cf=new CommentFactory(comment, ParseUser.getCurrentUser(),new Date(),poem);
        cf.Parcel();
        //Now Do The Notification Stuff also
        PostNotification(poem);
        Fragment fragment = new CommentFragment(this.currentpoem,this.shareflag,null);
        FragmentManager manager = getActivity().getSupportFragmentManager();

        manager.beginTransaction().replace(R.id.container, fragment).addToBackStack(null)
                .commit();
        //No Need To Add To Back Stack for This Moment
    }

    public void PostNotification(PoemsPost poem){
        //NotificationFactory nf=new NotificationFactory(ParseUser.getCurrentUser(),poem.getWriterUser(), StaticEnums.Notification.COMMENT.ordinal(),poem,new Date(),-1,poem.getTitle());
        //-1 is for comment Notification
       // nf.Parcel();
        FollowPost(poem);
        CloudPublish(poem);
        //PostsPublish(poem);
    }

    public void CloudPublish(PoemsPost poem){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("postid", poem.getObjectId());

        params.put("notificationcategory", String.valueOf(StaticEnums.Notification.FOLLOWCOMMENT.ordinal()));
        params.put("poemname", poem.getTitle());
        try {
            params.put("sendername",ParseUser.getCurrentUser().fetchIfNeeded().getString("DisplayName"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            params.put("userid",ParseUser.getCurrentUser().fetchIfNeeded().getObjectId());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ParseCloud.callFunctionInBackground("poemSubsribe", params, new FunctionCallback<String>() {
            @Override
            public void done(String arrays, ParseException e) {
                if (e == null) {
                    Log.d("Message Returned", arrays);

                } else {
                    e.printStackTrace();
                }
            }
        });
    }




    public  void FollowPost(PoemsPost poem){
        PostFollowCache postFollowCache=new PostFollowCache(getActivity().getApplicationContext());
        if(postFollowCache.getPostCache(poem.getObjectId())){
            //Already Subscribed no need to subscirbe
            Toast.makeText(getActivity().getApplicationContext(),"Already following the Poem",Toast.LENGTH_LONG).show();
        }else{
            new PostFollowWrapper(poem,ParseUser.getCurrentUser(),snackview).Follow();
        }
    }

    public  void PostsPublish(final PoemsPost poem){
        final List<String>tobeSent=new ArrayList<String>();
        ParseQuery<PoemsPost> check=PoemsPost.getQuery();
        check.whereContains("objectId",poem.getObjectId());
        check.findInBackground(new FindCallback<PoemsPost>() {
            @Override
            public void done(List<PoemsPost> list, ParseException e) {
                PoemsPost highlightedpoem = list.get(0);
                final JSONArray lists = highlightedpoem.getJSONArray("FollowersList");

                for (int i = 0; i < lists.length(); i++) {
                    JSONObject userobject = null;
                    try {
                        userobject = lists.getJSONObject(i);
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    Log.d("In Follow Loop", String.valueOf(i));
                    try {
                        if (ParseUser.getCurrentUser().getObjectId().equals(userobject.getString("UserId"))) {
                            //DO Nothing , Cant send notifictaion to Oneself Dude
                        } else {
                            tobeSent.add(userobject.getString("UserId"));
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                }//All Items are Allocated Now


                String[] userArr = new String[tobeSent.size()];
                userArr = tobeSent.toArray(userArr);

                ParseQuery<ParseUser> subscriptionQuery = ParseUser.getQuery();
                subscriptionQuery.whereContainedIn("objectId", Arrays.asList(userArr));
                subscriptionQuery.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> list, ParseException e) {
                        Log.d("Subscriptin Size", String.valueOf(list.size()));
                        for (ParseUser user : list) {
                            NotificationFactory nf = new NotificationFactory(ParseUser.getCurrentUser(), user, StaticEnums.Notification.FOLLOWCOMMENT.ordinal(), poem, new Date(), -1, poem.getTitle(), poem.getWriter());
                            nf.Parcel();
                        }
                    }
                });





            }
        });


    }
    @Override
    public void CommentPopUp() {
        AddComment();
    }

    @Override
    public void showRatings(PoemsPost poem,View root) {
        RatingsFragment ratings=new RatingsFragment(poem,root);
        ratings.show(getActivity().getSupportFragmentManager(), "Rate the Poem ");
    }

    public void showShareDialog(String social,PoemsPost source){
        ShareFragment shareFragment=new ShareFragment(social,source);
        shareFragment.show(getActivity().getSupportFragmentManager(), "Share The Poem");
    }
    @Override
    public void PoemShared(PoemsPost poem) {
        //Do The Facebook Sharing Dialog and Stuffs Here !!
        //For now , In app Notification
        NotificationFactory nf= null;
        try {
            nf = new NotificationFactory(ParseUser.getCurrentUser(),poem.getWriterUser(), StaticEnums.Notification.SHARE.ordinal(),poem,new Date(),-1,poem.getTitle(),ParseUser.getCurrentUser().fetchIfNeeded().getString("DisplayName"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        nf.Parcel();
    }

    @Override
    public void onSaveInstanceState(final Bundle outstate){
        Log.d("CHECK", "On Save Instance State");
        super.onSaveInstanceState(outstate);
        StaticConfiguration.setPoemsPost(currentpoem);
        outstate.putInt("STATE", 1);
    }

    @Override
    public void NotifyCommentSetChanged() {
        Fragment fragment = new CommentFragment(this.currentpoem,this.shareflag,null);
        FragmentManager manager = getActivity().getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.container, fragment).addToBackStack(null)
                .commit();
    }

    @Override
    public void NotifyPostDeleted() {
        Fragment fragment = new FeedsContainer();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.container, fragment).addToBackStack(null)
                .commit();

    }

    public void ShowAlertDialog(){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(),R.style.AppCompatAlertDialogStyle);

        builder.setTitle("Poem Delete ");
        builder.setMessage("Do You Really Want to Delete the Poem ?");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!shareflag) {
                    DeleteObject.DeletePost(getActivity().getApplicationContext(), currentpoem);
                } else {
                    DeleteObject.DeletePost(getActivity().getApplicationContext(), sharedpoem);
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }


    public void cleanAndAdd(List<CommentsPost>oldcache,List<CommentsPost>newcache,boolean clearflag) {
        if(clearflag){
            Log.d("Clearing","Old Datum");
            Log.d("Size of Old cache",String.valueOf(oldcache.size()));
            ParseObject.unpinAllInBackground(CommentTag,oldcache, new DeleteCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Log.d("Cleared", "Old Datum");
                    }
                }
            });
        }
        ParseObject.pinAllInBackground(CommentTag, newcache, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("Pinned", "Saved In Background");

                }
            }
        });
    }



}
