package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

/**
 * Created by pujan paudel on 9/14/2015.
 */
public interface DraftDetailCall {
    public  void openDetail(String title,String poembody,String category,int id);
}
