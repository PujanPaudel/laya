package com.nepalimutu.pujanpaudel.priceoverflow.model;

/**
 * Created by pujan paudel on 9/12/2015.
 */
public class Poems {
    public String poemtitle;
    public String writtenby;
    public String poembody;
    public String totalpoints;
    public String posteddate;
    public String poemcategory;
    public String objectid;
    public  Integer db_id;

    public Poems(){

    }
    public Poems(String poemtitle, String writtenby, String poembody, String totalpoints, String posteddate, String poemcategory,Integer dbid,String pid) {
        this.poemtitle = poemtitle;
        this.writtenby = writtenby;
        this.poembody = poembody;
        this.totalpoints = totalpoints;
        this.posteddate = posteddate;
        this.poemcategory = poemcategory;
        this.db_id=dbid;
        this.objectid=pid;
    }
}
