package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import com.nepalimutu.pujanpaudel.priceoverflow.R;

/**
 * Created by pujan paudel on 9/24/2015.
 */
public class SmallAvatarIndexes {

    public static int[] miniavatars=new int[]{
        R.drawable.firstt,
    R.drawable.secondd,
            R.drawable.thirdd,
            R.drawable.fourthh,
            R.drawable.fifthh,
            R.drawable.sixthh,
            R.drawable.seventhh,
            R.drawable.eightt,
            R.drawable.ninee,
            R.drawable.tenthh,
            R.drawable.eleventhh,
            R.drawable.twelvee,
            R.drawable.thirteenn,
            R.drawable.fourteenn,
            R.drawable.fifteenn,
            R.drawable.sixteenn

    };
}
