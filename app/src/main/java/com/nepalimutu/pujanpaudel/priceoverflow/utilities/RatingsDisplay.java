package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.view.View;

import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;

/**
 * Created by pujan paudel on 9/18/2015.
 */
public interface RatingsDisplay {
    public void showRatings(PoemsPost poem,View coordroot);
}
