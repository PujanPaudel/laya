package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

/**
 * Created by pujan paudel on 9/17/2015.
 */


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.nepalimutu.pujanpaudel.priceoverflow.model.Poems;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by pujan paudel on 9/13/2015.
 */

public class LikesCache extends SQLiteOpenHelper {

    //All Static VAriables
    //DATABASE VERSION
    private static  int DATABASE_VERSION=1;
    //Database NAme
    private static final String DATABASE_NAME="CACHE";
    //Database Table Name
    private static final String TABLE_CACHE="LIKES_CACHE";

    //Different Feeds for Different maybe

    private static final String KEY_ID="id";
    private static final String KEY_RATING="rating";
    private static final String KEY_OBJECTID="objectid";





    public LikesCache(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        // TODO Auto-generated constructor stub
    }

    //Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub


        String CREATE_FEED_TABLE="CREATE TABLE "+TABLE_CACHE+"("+KEY_ID+" INTEGER PRIMARY KEY,"+KEY_OBJECTID+" TEXT,"+KEY_RATING+" INTEGER"+")";

        Log.d("onCreate", CREATE_FEED_TABLE);
        db.execSQL(CREATE_FEED_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        Log.d("Wooh","On Upgrade");

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CACHE);
        onCreate(db);

    }

    //NOW ALL THE CRUD OPERATIONS

    //Addding a NEw Feed
    public void addRating(int  rating,String objid){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_RATING,rating);
        values.put(KEY_OBJECTID,objid);

        //Inserting row
        db.insert(TABLE_CACHE, null, values);
        db.close();
    }


    //For Getting Single Contact
    public boolean getRating(String id) {
//The ID is String
        SQLiteDatabase db = this.getReadableDatabase();
        //Query for getting a Single ID
        //Problem Here
        //get The reflection of full News Also !!
        Cursor cursor = db.query(TABLE_CACHE, new String[]{KEY_ID, KEY_RATING, KEY_OBJECTID}, KEY_OBJECTID + "=?", new String[]{id}, null, null, null, null);
        //THe Reflection Starts From 0

        if (cursor.getCount()==0) {
            return false; //got , its not empty
        } else {
            return true;// not got , its empty
        }


    }


}