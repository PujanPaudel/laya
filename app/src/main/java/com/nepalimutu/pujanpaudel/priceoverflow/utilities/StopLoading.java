package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

/**
 * Created by pujan paudel on 9/29/2015.
 */
public interface StopLoading {
    public void stopLoading();
    public void startLoading();
}
