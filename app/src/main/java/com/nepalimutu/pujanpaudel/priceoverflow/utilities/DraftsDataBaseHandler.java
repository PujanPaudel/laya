package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.nepalimutu.pujanpaudel.priceoverflow.model.Poems;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by pujan paudel on 9/13/2015.
 */

public class DraftsDataBaseHandler extends SQLiteOpenHelper {

    //All Static VAriables
    //DATABASE VERSION
    private static  int DATABASE_VERSION=1;
    //Database NAme
    private static final String DATABASE_NAME="POEM_DRAFTS";
    //Database Table Name
    private static final String TABLE_POEMS="DRAFT_POEMS";
    //Different Feeds for Different maybe

    private static final String KEY_ID="id";
    private static final String KEY_TITLE="title";
    private static final String KEY_WRITTENBY="writtenby";
    private static final String KEY_CATEGORY="poemcategory";
    private static final String KEY_FULL_POEM="fullpoem";
    private static final String KEY_DRAFT_DATE="draftdate";




    public DraftsDataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        // TODO Auto-generated constructor stub
    }

    //Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub


        String CREATE_FEED_TABLE="CREATE TABLE "+TABLE_POEMS+"("+KEY_ID+" INTEGER PRIMARY KEY,"+KEY_TITLE+" TEXT,"+KEY_WRITTENBY+" TEXT,"+KEY_CATEGORY+" TEXT,"+KEY_FULL_POEM+" TEXT,"+KEY_DRAFT_DATE+" TEXT"+")";
        Log.d("onCreate", CREATE_FEED_TABLE);
        db.execSQL(CREATE_FEED_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        Log.d("Wooh","On Upgrade");

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POEMS);
        onCreate(db);

    }

    //NOW ALL THE CRUD OPERATIONS

    //Addding a NEw Feed
    public void addPoem(Poems poem){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_TITLE,poem.poemtitle);
        values.put(KEY_WRITTENBY, poem.writtenby);
        values.put(KEY_CATEGORY,poem.poemcategory);
        values.put(KEY_FULL_POEM,poem.poembody);
        values.put(KEY_DRAFT_DATE,poem.posteddate);


        //Inserting row
        db.insert(TABLE_POEMS, null, values);
        db.close();
    }


    //For Getting Single Contact
    public Poems getFeed(String title) {

        SQLiteDatabase db=this.getReadableDatabase();
        //Query for getting a Single ID
        //Problem Here
        //get The reflection of full News Also !!
        Cursor cursor=db.query(TABLE_POEMS, new String[]{KEY_ID,KEY_TITLE,KEY_WRITTENBY,KEY_CATEGORY,KEY_FULL_POEM,KEY_DRAFT_DATE},KEY_TITLE+"=?", new String[]{title}, null, null, null,null);
            //THe Reflection Starts From 0

        if(cursor!=null)
            cursor.moveToFirst();



        Poems  poem=new Poems(cursor.getString(1), cursor.getString(2), cursor.getString(4),String.valueOf(-1),cursor.getString(5),cursor.getString(3),cursor.getInt(0),"-1");

        return poem;
    }

    public ArrayList<Poems> getAllFeeds() throws MalformedURLException{

        ArrayList<Poems> feedList=new ArrayList<Poems>();

        //0:Id Auto increment
        //1:Headline
        //2:LittlenEws
        //3:Category
        //4:Url
        //5:Feed ID


        String selectQuery="SELECT  * FROM " + TABLE_POEMS;
        //We need to use the Extra Query
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(selectQuery, null);
        //looping through all rows and adding the list
        if(cursor.moveToFirst()){
            do{
                Log.d("1",cursor.getString(1));
                Log.d("2",cursor.getString(2));
                Log.d("4",cursor.getString(4));
                Log.d("5",String.valueOf(cursor.getString(5)));

                //Do The Mapping of The Full NEws here Also

                Poems  poem=new Poems(cursor.getString(1), cursor.getString(2), cursor.getString(4),String.valueOf(-1),cursor.getString(5),cursor.getString(3),cursor.getInt(0),"-1");

                feedList.add(poem);
            }while(cursor.moveToNext());
        }
        return feedList;
    }



    public int getFeedCount(){
        String countQuery="SELECT * FROM "+ TABLE_POEMS;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(countQuery, null);
        int count=cursor.getCount();
        //Returning after Closing , WTF !!
        cursor.close();
        return count;


    }
    public void EditDraft(Poems poem,int id){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_TITLE,poem.poemtitle);
        values.put(KEY_WRITTENBY, poem.writtenby);
        values.put(KEY_CATEGORY,poem.poemcategory);
        values.put(KEY_FULL_POEM,poem.poembody);
        values.put(KEY_DRAFT_DATE,poem.posteddate);
        db.update(TABLE_POEMS, values, "id " + "=" + String.valueOf(id), null);
    }

    public void DeleteFavourite(int dbid){
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_POEMS, "id " + "=" + String.valueOf(dbid), null);
    }


}