package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

/**
 * Created by pujan paudel on 9/29/2015.
 */
public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener{

    public String TAG=EndlessRecyclerOnScrollListener.class.getSimpleName();

    private int previousTotal=0;
    private boolean loading=true;
    private int visibleThreshold=8;
    int firstvisibleItem,visibleItemCount,totalItemCount;

    private int current_page=1;
    private LinearLayoutManager mlinearlayoutmanager;

    public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager){
        this.mlinearlayoutmanager=linearLayoutManager;
    }


    @Override
    public void onScrolled(RecyclerView recyclerView,int dx,int dy)
    {
        super.onScrolled(recyclerView,dx,dy);

        visibleItemCount=recyclerView.getChildCount();
        totalItemCount=mlinearlayoutmanager.getItemCount();
        firstvisibleItem=mlinearlayoutmanager.findFirstVisibleItemPosition();


        if(loading){
            if(totalItemCount>previousTotal){
                loading=false;
                previousTotal=totalItemCount;
            }

        }

    if(!loading
            && (totalItemCount-visibleItemCount)<=(firstvisibleItem+visibleThreshold)){
        //End has been reached
        //Do Something
        //currentskip+=5
        //retrieveList
onLoadMore();
        loading=true;
    }



    }
public abstract void onLoadMore();

}
