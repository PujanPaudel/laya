package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.nepalimutu.pujanpaudel.priceoverflow.Application;
import com.nepalimutu.pujanpaudel.priceoverflow.Login;

/**
 * Created by pujan paudel on 11/26/2015.
 */
public class RedirectToRegister {

    public static void SnackShow(View mview, final Activity mactivity){
        Snackbar snackbar=Snackbar.make(mview, "Feature...", Snackbar.LENGTH_LONG);
        //Adding action to snackbar
        snackbar.setAction("Register", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Displaying another snackbar when user click the action for first snackbar
                Application.resetGuestUser(mactivity.getApplicationContext());
                Intent register=new Intent(mactivity,Login.class);
                mactivity.startActivity(register);


            }
        });

        //Customizing colors
        snackbar.setActionTextColor(Color.BLUE);
        View view = snackbar.getView();
        TextView textView = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.RED);

        //Displaying snackbar
        snackbar.show();
    }
}
