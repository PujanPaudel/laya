package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.view.View;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by pujan paudel on 9/18/2015.
 */
public class PersonalInfoWrapper {
    private String interested,besauthor,bestquote,bestbooks;
    private ParseUser currentuser;
    private View coordinator;


    public PersonalInfoWrapper(String interested, String besauthor, String bestquote, String bestbooks, ParseUser currentuser,View v) {
        this.interested = interested;
        this.besauthor = besauthor;
        this.bestquote = bestquote;
        this.bestbooks = bestbooks;
        this.currentuser = currentuser;
        this.coordinator=v;
    }
    public  void Update(){
        currentuser.put("bestauthor",besauthor);
        currentuser.put("bestquote",bestquote);
        currentuser.put("bestbooks",bestbooks);
        currentuser.put("Interested",interested);
        currentuser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                SnackbarShow.MySnackbarShow("Updated the Info",coordinator);
            }
        });
    }
}
