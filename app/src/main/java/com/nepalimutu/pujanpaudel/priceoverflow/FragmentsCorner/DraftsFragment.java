package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.nepalimutu.pujanpaudel.priceoverflow.Adapters.DraftsAdapter;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.DraftsDataBaseHandler;

import java.net.MalformedURLException;

/**
 * Created by pujan paudel on 9/13/2015.
 */
public class DraftsFragment extends Fragment {
    private RecyclerView recList;
    private RecyclerView yesdatalayout;
    private LinearLayout nodatalayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_feed, container, false);
        recList = (RecyclerView)view.findViewById(R.id.cardList);

        Button refreshbutton=(Button)view.findViewById(R.id.refresh);
        refreshbutton.setVisibility(View.GONE);

        FloatingActionButton addpost=(FloatingActionButton)view.findViewById(R.id.addPost);
        addpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewPost();
            }

        });
        recList.setHasFixedSize(false);
        yesdatalayout=recList;
        nodatalayout=(LinearLayout)view.findViewById(R.id.nodataScreen);
        nodatalayout.setVisibility(View.GONE);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        DraftsDataBaseHandler db=new DraftsDataBaseHandler(getActivity());

        DraftsAdapter da = null;
        try {
            da = new DraftsAdapter(db.getAllFeeds());
            if(da.getItemCount()==0){
                nodatalayout.setVisibility(View.VISIBLE);
                yesdatalayout.setVisibility(View.GONE);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        recList.setAdapter(da);

        return view;
    }

    public void NewPost(){
        Fragment fragment = new NewPostFragment();
        FragmentManager manager = getActivity().getSupportFragmentManager();

        manager.beginTransaction().replace(R.id.container, fragment)
                .addToBackStack(null).commit();

    }

}
