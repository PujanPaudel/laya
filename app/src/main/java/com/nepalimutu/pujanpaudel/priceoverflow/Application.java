package com.nepalimutu.pujanpaudel.priceoverflow;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.ReportPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.CommentsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.FeedBack;
import com.nepalimutu.pujanpaudel.priceoverflow.model.FollowList;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Notifications;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Reports;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.PushService;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import io.fabric.sdk.android.DefaultLogger;
import io.fabric.sdk.android.Fabric;

/**
 * Created by pujan paudel on 9/14/2015.
 */
//@ReportsCrashes(formUri = "http://www.yourselectedbackend.com/reportpath")
public class Application extends android.app.Application{

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "ZdKUyoUTfcVhbtjEVbQQ6Va7m";
    private static final String TWITTER_SECRET = "Z7ICBmhNH76LKCrdLjInmI4BG9CnEX7CUENhQItd7eIqH5hpdc";


    @Override

    public void onCreate(){

        super.onCreate();
        //ACRA.init(this);
        //Fabric.with(this, new Twitter(authConfig));
        Log.d("APPLICATION", "OnCreate");
        FacebookSdk.sdkInitialize(getApplicationContext());
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Twitter(authConfig))
                .logger(new DefaultLogger(Log.DEBUG))
                .debuggable(true)
                .build();
        Fabric.with(fabric);


        Parse.enableLocalDatastore(this);

        Parse.initialize(this, "R8LFhh9nhsXrbRC7yXsbtgj2fuEDToc291zWVc8Z", "TjgvcHdkeTdllxZuhEZjCZIV9noMoBah1VoO3F5w");
        ParseUser.enableRevocableSessionInBackground();


        ParseUser.enableAutomaticUser();

        ParseACL defaultACL=new ParseACL();
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);
        ParseObject.registerSubclass(PoemsPost.class);
        ParseObject.registerSubclass(CommentsPost.class);
        ParseObject.registerSubclass(Notifications.class);
        ParseObject.registerSubclass(FollowList.class);
        ParseObject.registerSubclass(FeedBack.class);
        ParseObject.registerSubclass(Reports.class);


        //The Push Service Registration Part  :)
        if(userSet()){
            ParseInstallation pi= ParseInstallation.getCurrentInstallation();

            PushService.setDefaultPushCallback(this, HomeActivity.class);
            Context ctx=getApplicationContext();
            PushService.subscribe(ctx,"Simple",HomeActivity.class);

        }
        Log.d("APPLICATION","OnCreate Ended ");
    }


    public boolean userSet(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return sp.getBoolean("Registered",false);
    }
    public static void IncreaseNotifications(Context ctx){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        int current=sp.getInt("Count",0);
        current++;
        sp.edit().putInt("Count",current).commit();

    }

    public static void ClearNotificatiosn(Context ctx){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        sp.edit().putInt("Count",0).commit();
    }

    public static boolean showNotifications(Context ctx){
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        return sharedPrefs.getBoolean("prefSendNotifications",true);
    }

    public static  boolean isGuestUser(Context ctx){
        SharedPreferences sharedPreferences=PreferenceManager.getDefaultSharedPreferences(ctx);
        return sharedPreferences.getBoolean("isGuest",false);
    }

    public static void setGuestUser(Context ctx){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        sp.edit().putBoolean("isGuest", true).commit();
    }
    public static void resetGuestUser(Context ctx){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        sp.edit().putBoolean("isGuest", false).commit();
    }
}
