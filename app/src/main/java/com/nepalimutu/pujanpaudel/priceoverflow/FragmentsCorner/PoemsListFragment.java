package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.Adapters.PostAdapter;
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.NetworkStatus;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Poems;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.EndlessRecyclerOnScrollListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pujan paudel on 9/18/2015.
 */
public class PoemsListFragment extends Fragment {
    private RecyclerView recList;
    private PostAdapter postAdapter;
    private List<PoemsPost> feedposts=new ArrayList<PoemsPost>();
    private ParseUser targetuser;
    private int currentskip=0;
    private LinearLayoutManager llm;
    private LinearLayout nodatascreen;

    public PoemsListFragment(){

    }
    @SuppressLint("ValidFragment")
    public  PoemsListFragment(ParseUser user){
        this.targetuser=user;
    }
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_feed, container, false);
        recList = (RecyclerView)view.findViewById(R.id.cardList);
        TextView errorType=(TextView)view.findViewById(R.id.dataType);
        errorType.setText("No Poems Available");
        Button refreshbutton=(Button)view.findViewById(R.id.refresh);

        recList.setHasFixedSize(false);
         llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        nodatascreen=(LinearLayout)view.findViewById(R.id.nodataScreen);
        nodatascreen.setVisibility(View.GONE);

        refreshbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retrieveList();
            }
        });

        FloatingActionButton addpost=(FloatingActionButton)view.findViewById(R.id.addPost);
        try {
            if(targetuser.fetchIfNeeded().getObjectId()==ParseUser.getCurrentUser().fetchIfNeeded().getObjectId()){
            addpost.setVisibility(View.VISIBLE);
            }else{
                addpost.setVisibility(View.GONE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        addpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewPost();
            }

        });


        postAdapter = new PostAdapter(feedposts);
        recList.setAdapter(postAdapter);
        if(NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())){
            retrieveList();
        }
        else{
            nodatascreen.setVisibility(View.VISIBLE);
            recList.setVisibility(View.GONE);

        }
        return view;

    }


    private void retrieveList(){
        nodatascreen.setVisibility(View.GONE);
        recList.setVisibility(View.VISIBLE);
        ParseQuery<PoemsPost> poemsquery = PoemsPost.getQuery();
        poemsquery.orderByDescending("createdAt");
        poemsquery.whereEqualTo("WriterUser", targetuser);
        poemsquery.include("user");
        poemsquery.setLimit(5);
        poemsquery.setSkip(currentskip);
        postAdapter.startLoading();
        poemsquery.findInBackground(new FindCallback<PoemsPost>() {
            @Override
            public void done(List<PoemsPost> list, ParseException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "Some Network Problem", Toast.LENGTH_LONG).show();
                    nodatascreen.setVisibility(View.VISIBLE);
                    recList.setVisibility(View.GONE);
                } else {
                    Log.d("Items", String.valueOf(list.size()));
                    for (PoemsPost poems : list) {
                        feedposts.add(poems);
                    }
                    if (feedposts.size() == 0) {
                        nodatascreen.setVisibility(View.VISIBLE);
                        recList.setVisibility(View.GONE);
                    }
                    postAdapter.notifyDataSetChanged();
                    postAdapter.stopLoading();
                    recList.scrollBy(1, 0);

                    if (!NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())) {
                        return;
                    }

                    recList.addOnScrollListener(new EndlessRecyclerOnScrollListener(llm) {
                        @Override
                        public void onLoadMore() {
                            currentskip += 5;
                            retrieveList();
                            recList.removeOnScrollListener(this);
                        }
                    });


                }
            }
        });
        return;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        //StaticConfiguration.setSearchesList(searchlists);
    }

    public void NewPost(){

        Fragment fragment = new NewPostFragment("","","Poem",-1);
        FragmentManager manager = getActivity().getSupportFragmentManager();

        manager.beginTransaction().replace(R.id.container, fragment)
                .addToBackStack(null).commit();

    }
}
