package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nepalimutu.pujanpaudel.priceoverflow.R;

/**
 * Created by pujan paudel on 9/14/2015.
 */
public class PoemDetailFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.poem_offline_read, container, false);
        return view;
    }
}
