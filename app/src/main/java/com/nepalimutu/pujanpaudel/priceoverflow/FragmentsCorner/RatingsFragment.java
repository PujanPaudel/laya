package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.LikesCache;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.NotificationFactory;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StaticEnums;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.SnackbarShow;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by pujan paudel on 9/17/2015.
 */
@SuppressLint("ValidFragment")
public class RatingsFragment extends DialogFragment implements AdapterView.OnItemClickListener {
    private ListView categories;
    private  String[] ratingsarray;
    private int rating;
    private PoemsPost poembody;
    private View snackview;
    private RatingsFragment reference;

    @SuppressLint("ValidFragment")

    public RatingsFragment(PoemsPost mypoem,View root){
        this.poembody=mypoem;
        this.snackview=root;

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        ratingsarray=new String[]{"1 -Poor ","2 - Satisfactory ","3 -Good ","4 - Awesome","5 - OutStanding"};
        reference=this;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ratings_dialog, container,
                false);
        getDialog().setTitle("Rating For Poem");
        categories = (ListView) rootView.findViewById(R.id.listView);
        categories.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_multiple_choice, ratingsarray));

        categories.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        categories.setOnItemClickListener(this);
        Button rate=(Button)rootView.findViewById(R.id.rate);
        rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final LikesCache likecache = new LikesCache(HomeActivity.reference.getApplicationContext());
                RatingsFragment.this.dismiss();

                poembody.increment("Points", rating);
                poembody.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            SnackbarShow.MySnackbarShow("Rated the Poem : "+String.valueOf(rating), snackview);
                            likecache.addRating(rating, poembody.getObjectId());

                            try {
                                SendNotification();
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }
                        } else {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        return rootView;
    }

    public void SendNotification() throws ParseException {
        NotificationFactory nf=new NotificationFactory(ParseUser.getCurrentUser(),poembody.getWriterUser(), StaticEnums.Notification.RATE.ordinal(),poembody,new Date(),rating,poembody.getTitle(),ParseUser.getCurrentUser().fetchIfNeeded().getString("DisplayName"));
        nf.Parcel();


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("recieverid",poembody.getWriterUser().fetchIfNeeded().getUsername());
         params.put("notificationcategory",String.valueOf(2));
         Log.d("Calling","Cloud Function");
         ParseCloud.callFunctionInBackground("ratesharePush", params, new FunctionCallback<String>() {
        @Override
        public void done(String s, ParseException e) {
        }
        });





        /**
         *  ParseQuery query = ParseInstallation.getQuery();

         try {
         // query.whereEqualTo("device_id",mb.getOwner().fetchIfNeeded().getUsername().toString());
         query.whereEqualTo("device_id",poembody.getWriterUser().fetchIfNeeded().getUsername());
         Log.d("FEtched",poembody.getWriterUser().fetchIfNeeded().getObjectId());
         } catch (ParseException e1) {
         // TODO Auto-generated catch block
         e1.printStackTrace();
         }

         ParsePush push = new ParsePush();
         JSONObject data = getJSONNotification();
         push.setData(data);
         */



    }

    private JSONObject getJSONNotification() {
        return null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                rating=position+1;
            //HAA HAAA, The index starts from 0 while the Points Start From 1
    }
}
