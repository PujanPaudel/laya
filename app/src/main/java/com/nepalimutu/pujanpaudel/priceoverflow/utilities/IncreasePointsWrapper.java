package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.util.Log;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pujan paudel on 10/1/2015.
 */
public class IncreasePointsWrapper {
    private ParseUser tobeincreased;
    private int points;
    public IncreasePointsWrapper(ParseUser user,int pnts){
        tobeincreased=user;
        points=pnts;
    }
    public void CloudIncrease(){
        Map<String,String> params=new HashMap<String,String>();
        try {
            params.put("userid",tobeincreased.fetchIfNeeded().getObjectId());
            params.put("points",String.valueOf(points));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ParseCloud.callFunctionInBackground("increasePoints", params, new FunctionCallback<String>() {
            @Override
            public void done(String arrays, ParseException e) {
                if (e == null) {
                    //Success Error Not Called Problem
                } else {
                    e.printStackTrace();
                }
            }
        });



    }
}
