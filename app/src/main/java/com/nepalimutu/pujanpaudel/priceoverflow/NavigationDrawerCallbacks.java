package com.nepalimutu.pujanpaudel.priceoverflow;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
