package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.Adapters.NotificationAdapter;
import com.nepalimutu.pujanpaudel.priceoverflow.Application;
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.Interfaces.DrawerReadyCallBack;
import com.nepalimutu.pujanpaudel.priceoverflow.NavigationDrawerFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.NetworkStatus;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.CommentsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Notifications;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.EndlessRecyclerOnScrollListener;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pujan paudel on 9/16/2015.
 */
public class NotificationFragment extends Fragment implements DrawerReadyCallBack{
    private RecyclerView recList;
    private List<Notifications>notificationsFeed=new ArrayList<Notifications>();
    private List<Notifications>localposts;
    private NotificationAdapter postadapter;
    private int currentskip=0;
    private LinearLayoutManager llm;
    public static  NotificationFragment reference;
    public static  String NotificationTag="Notification";
    private RecyclerView yesdatalayout;
    private LinearLayout nodatalayout;
    private TextView datatype;
    private Button refreshbutton;
    @Override
    public void onCreate(Bundle savedInstaceState){
        super.onCreate(savedInstaceState);
        reference=this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Application.ClearNotificatiosn(getActivity().getApplicationContext());
        currentskip=0;
        View view = inflater.inflate(R.layout.activity_feed, container, false);
        recList = (RecyclerView)view.findViewById(R.id.cardList);
        yesdatalayout=recList;
        nodatalayout=(LinearLayout)view.findViewById(R.id.nodataScreen);
        datatype=(TextView)view.findViewById(R.id.dataType);
        refreshbutton=(Button)view.findViewById(R.id.refresh);
        FloatingActionButton addpost=(FloatingActionButton)view.findViewById(R.id.addPost);
        addpost.setVisibility(View.INVISIBLE);
        nodatalayout.setVisibility(View.INVISIBLE);


        recList.setHasFixedSize(false);


        //OnClickListener For Refresh
        refreshbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retrieveList(true);
            }
        });
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        postadapter=new NotificationAdapter(notificationsFeed);
        recList.setAdapter(postadapter);
        postadapter.startLoading();
        ParseQuery<Notifications> notificationsquery = Notifications.getQuery();
        notificationsquery.orderByDescending("createdAt");
        notificationsquery.include("user");
        notificationsquery.include("Poems");
        notificationsquery.fromPin(NotificationTag);
        notificationsquery.whereEqualTo("Reciever", ParseUser.getCurrentUser());
        notificationsquery.setLimit(20);
        notificationsquery.findInBackground(new FindCallback<Notifications>() {
            @Override
            public void done(List<Notifications> list, ParseException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "Some Network Problem", Toast.LENGTH_LONG).show();

                } else {
                    for (Notifications noti : list) {
                        notificationsFeed.add(noti);
                    }
                    localposts = new ArrayList<Notifications>(notificationsFeed);
                    postadapter.notifyDataSetChanged();
                    postadapter.stopLoading();
                    recList.scrollBy(1, 0);

                    if(NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())){
                        retrieveList(true);
                    }else{
                        if(list.size()==0){
                            nodatalayout.setVisibility(View.VISIBLE);
                            yesdatalayout.setVisibility(View.INVISIBLE);
                            datatype.setText("No Notifications Available");
                        }
                    }
                }
            }
        });


      //  NavigationDrawerFragment.reference.resetNotificationMenu();
        //HomeActivity.reference.mNavigationDrawerFragment.resetNotificationMenu();// Clear it For Now
        return view;
    }


    private void retrieveList(final boolean clearflag){
        nodatalayout.setVisibility(View.INVISIBLE);
        yesdatalayout.setVisibility(View.VISIBLE);
        datatype.setText("No Poems Available");
        ParseQuery<Notifications> notificationsquery = Notifications.getQuery();
        notificationsquery.setLimit(5);//4 is just a placeholder value for now
        postadapter.startLoading();
        notificationsquery.orderByDescending("createdAt");
        notificationsquery.include("user");
        notificationsquery.include("Poems");
        notificationsquery.setSkip(currentskip);
        notificationsquery.whereEqualTo("Reciever", ParseUser.getCurrentUser());
        notificationsquery.findInBackground(new FindCallback<Notifications>() {
            @Override
            public void done(List<Notifications> list, ParseException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "Some Network Problem", Toast.LENGTH_LONG).show();
                    nodatalayout.setVisibility(View.VISIBLE);
                    yesdatalayout.setVisibility(View.INVISIBLE);
                    datatype.setText("No Notifications Available");
                } else {
                    if(clearflag) {
                        notificationsFeed.clear();
                        Log.d("Items", String.valueOf(list.size()));
                        for (Notifications notifications : list) {
                            notificationsFeed.add(notifications);
                            if(list.size()==0){
                                currentskip += 5;
                            }
                        }
                        if (notificationsFeed.size() == 0) {
                            nodatalayout.setVisibility(View.VISIBLE);
                            yesdatalayout.setVisibility(View.GONE);
                            datatype.setText("No Notifications Available");
                        }
                        postadapter.notifyDataSetChanged();
                    }
                    postadapter.stopLoading();
                    cleanAndAdd(localposts, notificationsFeed, clearflag);
                    recList.scrollBy(1, 0);
                    recList.addOnScrollListener(new EndlessRecyclerOnScrollListener(llm) {
                        @Override
                        public void onLoadMore() {
                            retrieveList(false);
                            recList.removeOnScrollListener(this);
                        }
                    });

                    //The Thug method of Notify data Set changed :D

                }
            }
        });
        return;
    }


    public void cleanAndAdd(List<Notifications>oldcache,List<Notifications>newcache,boolean clearflag) {
        if(clearflag){
            Log.d("Clearing","Old Datum");
            Log.d("Size of Old cache",String.valueOf(oldcache.size()));
            ParseObject.unpinAllInBackground(NotificationTag,oldcache, new DeleteCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Log.d("Cleared", "Old Datum");
                    }
                }
            });
        }
        ParseObject.pinAllInBackground(NotificationTag,newcache, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("Pinned", "Saved In Background");

                }
            }
        });
    }

    @Override
    public void DrawerReady() {
        Toast.makeText(HomeActivity.reference.getApplicationContext(),"Drawer is Ready Boss",Toast.LENGTH_LONG).show();
        Log.d("Yahoo","Drawer Ready");

    }
}
