package com.nepalimutu.pujanpaudel.priceoverflow;

/**
 * Created by pujan paudel on 9/16/2015.
 */
public interface DataSetNotifyCallback {
    public void Notify();
}
