package com.nepalimutu.pujanpaudel.priceoverflow.utilities;
import android.content.Context;
import android.widget.Toast;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.CommentFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.model.CommentsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.parse.DeleteCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.HashMap;

/**
 * Created by pujan paudel on 10/2/2015.
 */
public class DeleteObject {

    public static void DeleteComment(final Context ctx,CommentsPost myobject) {
        myobject.deleteInBackground(new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(ctx, "Deleted the Comment Succesfully", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(ctx, "Some Network Error", Toast.LENGTH_LONG).show();
                }
                CommentFragment.reference.NotifyCommentSetChanged();
        }
    });

    }

    public static void DeletePost(final Context ctx, final PoemsPost mypoem){
       mypoem.deleteInBackground(new DeleteCallback() {
           @Override
           public void done(ParseException e) {
               if (e == null) {
                   Toast.makeText(ctx, "Deleted the Poem Succesfully", Toast.LENGTH_LONG).show();
                   if(mypoem.getPostType()==0){//0 is for Original , 1 is for Shared
                       //Inference : Type Shared Poem
                   new IncreasePointsWrapper(ParseUser.getCurrentUser(), -20).CloudIncrease();
                   HashMap<String, String> params = new HashMap<String, String>();
                   params.put("points", String.valueOf(-1));
                   try {
                       params.put("userid", ParseUser.getCurrentUser().fetchIfNeeded().getObjectId());
                   } catch (ParseException e1) {
                       e1.printStackTrace();
                   }
                   ParseCloud.callFunctionInBackground("increasePoems", params, new FunctionCallback<String>() {
                       @Override
                       public void done(String s, ParseException e) {
                       }
                   });

                   }
               } else {
                   Toast.makeText(ctx, "Some Network Error", Toast.LENGTH_LONG).show();

               }
               CommentFragment.reference.NotifyPostDeleted();
           }
       });
    }
}


//Problem Here : Shouldn't Do  the Decrease Poems and Decrease points on teh Shared Types of poems
//hahaha, Such a Silly bug Pig