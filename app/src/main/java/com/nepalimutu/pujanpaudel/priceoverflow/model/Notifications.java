package com.nepalimutu.pujanpaudel.priceoverflow.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Date;

/**
 * Created by pujan paudel on 9/16/2015.
 */
@ParseClassName("Notifications")
public class Notifications extends ParseObject {
    /**
     * @Params
     * @ModelPart
     * Sender-ParseUser
     * Reciever-ParseUser
     * NotificationType-Enum or Integer
     * Poem Associated -PoemsPost Object ( Pointer )
     * NotificationDate
     */
    public void setSender(ParseUser sender){
        put("Sender",sender);
    }
    public ParseUser getSender(){
        return getParseUser("Sender");
    }

    public void setReciever(ParseUser reciever){
        put("Reciever",reciever);
    }
    public ParseUser getReciever(){
        return getParseUser("Reciever");
    }

    public void setCategory(int category){
        put("Category",category);
    }
    public int getCategory(){
        return getInt("Category");
    }

    public void setPoem(PoemsPost poem){
        put("Poem",poem);
    }
    public ParseObject getPoem(){
        return getParseObject("Poem");
    }

    public void setDate(Date date){
        put("NotificationDate",date);
    }

    public Date getDate(){
        return getDate("NotificationDate");
    }

    public void setRating(int rating){
        put("Rating",rating);
    }

    public int getRating(){
        return getInt("Rating");
    }
    public void setPoemName(String name){
        put("PoemName",name);
    }

    public String getPoemName(){
        return getString("PoemName");
    }

    public void setSenderName(String senderNamee){
        put("SenderName",senderNamee);
    }

    public String getSenderName(){
        return getString("SenderName");
    }
    public static ParseQuery<Notifications> getQuery(){
        return ParseQuery.getQuery(Notifications.class);
    }
}

