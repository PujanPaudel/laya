package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by pujan paudel on 10/24/2015.
 */
public class ReportCache extends SQLiteOpenHelper{
    private static  int DATABASE_VERSION=1;
    //Database NAme
    private static final String DATABASE_NAME="REPORT_CACHE";
    //Database Table Name
    private static final String TABLE_CACHE="REPORT_CACHE";
    private static final String KEY_ID="id";
    private static final String KEY_OBJECTID="objectid";


    public ReportCache(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_FEED_TABLE="CREATE TABLE "+TABLE_CACHE+"("+KEY_ID+" INTEGER PRIMARY KEY,"+KEY_OBJECTID+" TEXT"+")";

        Log.d("onCreate", CREATE_FEED_TABLE);
        db.execSQL(CREATE_FEED_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        Log.d("Wooh","On Upgrade");

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CACHE);
        onCreate(db);

    }

    public void addReport(String objid){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_OBJECTID, objid);
        //Inserting row
        db.insert(TABLE_CACHE, null, values);
        db.close();
    }


    public boolean getReport(String poemid){
        SQLiteDatabase db=this.getReadableDatabase();
        //Query for getting a Single ID
        //Problem Here
        //get The reflection of full News Also !!
        Cursor cursor=db.query(TABLE_CACHE, new String[]{KEY_ID,KEY_OBJECTID},KEY_OBJECTID+"=?", new String[]{poemid}, null, null, null,null);

        //THe Reflection Starts From 0

        if(cursor.getCount()==0){
            db.close();
            return false; //its empty
        }else{
            db.close();
            return true;
        }
    }
}
