package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.nepalimutu.pujanpaudel.priceoverflow.Adapters.UserSearchAdapter;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StaticConfiguration;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by pujan paudel on 9/22/2015.
 */
public class UserSearchFragment extends Fragment {

    private RecyclerView recList;
    private UserSearchAdapter usersearch;
    private List<ParseUser> searchlists;
    private RecyclerView yesdatalayout;
    private LinearLayout nodatalayout;

    public UserSearchFragment(){
        Log.d("Empty","Constructor Called");
        this.searchlists=StaticConfiguration.getList();
    }

    @SuppressLint("ValidFragment")
    public UserSearchFragment(List<ParseUser>searches){
        this.searchlists=searches;
        Log.d("I Recieved",String.valueOf(searches.size()));
    }
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_feed, container, false);
        recList=(RecyclerView)rootView.findViewById(R.id.cardList);
        Button refreshbutton=(Button)rootView.findViewById(R.id.refresh);
        refreshbutton.setVisibility(View.GONE);
        FloatingActionButton addpost=(FloatingActionButton)rootView.findViewById(R.id.addPost);
        addpost.setVisibility(View.GONE);
        recList.setHasFixedSize(false);
        yesdatalayout=recList;
        nodatalayout=(LinearLayout)rootView.findViewById(R.id.nodataScreen);
        nodatalayout.setVisibility(View.GONE);
        if(searchlists.size()==0){
            nodatalayout.setVisibility(View.VISIBLE);
            yesdatalayout.setVisibility(View.GONE);
        }

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        final CoordinatorLayout mylayout=(CoordinatorLayout)rootView.findViewById(R.id.coordinate);
        usersearch=new UserSearchAdapter(this.searchlists,mylayout);
        recList.setAdapter(usersearch);
    return rootView;
    }
@Override
    public void onSaveInstanceState(Bundle savedInstanceState){
    super.onSaveInstanceState(savedInstanceState);
    //StaticConfiguration.setSearchesList(searchlists);
}
}
