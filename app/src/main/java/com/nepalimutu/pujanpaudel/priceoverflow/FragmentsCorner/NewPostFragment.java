package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.Interfaces.PoemAddedCallback;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.FollowList;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Poems;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.DraftsDataBaseHandler;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.IncreasePointsWrapper;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.NotificationFactory;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.PostFollowWrapper;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StaticEnums;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseACL;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pujan paudel on 9/12/2015.
 */
public class NewPostFragment extends Fragment implements PoemAddedCallback {
    private String[] poemtitles;
    private Button savebutton,draftbutton;
    private EditText poemtitle,poembody;
    private String poemcategory;
    private String spoemtitle,spoembody;
    private int db_id;
    public  NewPostFragment(){


    }
    @SuppressLint("ValidFragment")
    public NewPostFragment(String stit,String spobdy,String cat,int dbid){
        this.spoemtitle=stit;
        this.spoembody=spobdy;
        this.poemcategory=cat;
        this.db_id=dbid;

        Log.d("DB_ID",String.valueOf(dbid));
    }
    @Override
    public void onCreate(Bundle savedInstancstate){
        super.onCreate(savedInstancstate);
        this.poemtitles = new String[] {
                "Poem", "Haiku", "Article", "Gajals", "Miscellaneous"
        };
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_post_view, container, false);
        Spinner s = (Spinner)view.findViewById(R.id.spinner_poemcategory);
        poemtitle =(EditText)view.findViewById(R.id.edt_poemtitle);
        poembody =(EditText)view.findViewById(R.id.edt_poembody);
        poemtitle.setText(spoemtitle);
        poembody.setText(spoembody);

        savebutton=(Button)view.findViewById(R.id.btn_post);
        draftbutton=(Button)view.findViewById(R.id.btn_draft);
        s.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, poemtitles);
        s.setAdapter(adapter);
        s.setSelection(adapter.getPosition(poemcategory));
        savebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SavePoem();
            }
        });
        draftbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DraftPoem();
            }
        });
        return view;


    }

    @Override
    public void BackToFeed() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        manager.popBackStackImmediate();

    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            Toast.makeText(parent.getContext(),
                    "Category Selected : " + parent.getItemAtPosition(pos).toString(),
                    Toast.LENGTH_SHORT).show();

            poemcategory=parent.getItemAtPosition(pos).toString();
            // NOw We need to Do Fucking Awesome Things HEre
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }
    }
    public void SavePoem(){

        if(poemtitle.getText().toString().trim().length()==0){
            poemtitle.requestFocus();
            poemtitle.setError("Poem Title Can't be Empty");
            return;
        }
        if(poembody.getText().toString().trim().length()==0){
            poembody.requestFocus();
            poembody.setError("Poem Body Can't be Empty");
            return;
        }

        final PoemsPost newPost=new PoemsPost();
        newPost.setCategory(poemcategory);
        newPost.setDate(new Date());
        newPost.setFullPoem(poembody.getText().toString());
        newPost.setTitle(poemtitle.getText().toString());
        try {
            newPost.setWriter(ParseUser.getCurrentUser().fetchIfNeeded().getString("DisplayName"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        newPost.setWriterUser(ParseUser.getCurrentUser());
        newPost.setPoints(0);
        newPost.setFollowersArray();
        newPost.setPostType(0); //0 is for Normal Type  //1  is for third party share
        //NO need for Third party ;) Let it be Null
        ParseACL acl=new ParseACL();
        acl.setPublicReadAccess(true);
        acl.setPublicWriteAccess(true);
        newPost.setACL(acl);

        final ProgressDialog saving=new ProgressDialog(getActivity());

       saving.setMessage("Posting The Poem");
        saving.show();

          newPost.saveInBackground(new SaveCallback() {
              @Override
              public void done(ParseException e) {
                  if (e == null) {
                      saving.dismiss();

                      new PostFollowWrapper(newPost, ParseUser.getCurrentUser(), null).Follow();
                      new IncreasePointsWrapper(ParseUser.getCurrentUser(), 20).CloudIncrease();
                      try {
                          IncreasePoems();
                      } catch (ParseException e1) {
                          e1.printStackTrace();
                      }
                      Toast.makeText(getActivity().getApplicationContext(), "You Got 20 Points for Posting  the Poem ", Toast.LENGTH_LONG).show();
                      HashMap<String, String> params = new HashMap<String, String>();
                      params.put("postid", newPost.getObjectId());
                      params.put("notificationcategory", String.valueOf(StaticEnums.Notification.FOLLOWPOST.ordinal()));
                      params.put("poemname",newPost.getTitle());
                      try {
                          params.put("sendername",ParseUser.getCurrentUser().fetchIfNeeded().getString("DisplayName"));
                      } catch (ParseException e1) {
                          e1.printStackTrace();
                      }
                      ParseCloud.callFunctionInBackground("userSubscribe", params, new FunctionCallback<String>() {
                          @Override
                          public void done(String arrays, ParseException e) {

                              if (e == null) {
                                  Log.d("Message Returned", arrays);

                              } else {
                                  e.printStackTrace();
                              }
                          }
                      });


                      // PublishToFollowers(newPost);
                      BackToFeed();// Go Back to Feed now

                  }
                  else{
                      Toast.makeText(getActivity(), "Err!! Some Network Problem Try Again Later", Toast.LENGTH_SHORT).show();
                  }
              }
          });

         newPost.pinInBackground(new SaveCallback() {
             @Override
             public void done(ParseException e) {
                 if (e == null) {
                     Log.d("Cool", "Object Pinned ");
                 }
             }
         });

    }
    public  void DraftPoem(){
        Date date =new Date();

        if(this.db_id>=0){
            //Do the Editing part
            int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME|  DateUtils.FORMAT_NO_YEAR;
            long millisecond =date.getTime();
            String monthAndDayText = DateUtils.formatDateTime(HomeActivity.reference.getApplicationContext(),millisecond, flags);

            Toast.makeText(getActivity().getApplicationContext(),"Edit The Poem",Toast.LENGTH_LONG).show();
            DraftsDataBaseHandler db=new DraftsDataBaseHandler(getActivity());
            db.EditDraft(new Poems(poemtitle.getText().toString(), "Pujan paudel", poembody.getText().toString(), String.valueOf(0),monthAndDayText, poemcategory,-1,"-1"),this.db_id);
            FragmentManager manager = getActivity().getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.container,new DraftsFragment()).commit();
        }else{
            //Do The Drafting part
            int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME|  DateUtils.FORMAT_NO_YEAR;
            long millisecond =date.getTime();
            String monthAndDayText = DateUtils.formatDateTime(HomeActivity.reference.getApplicationContext(), millisecond, flags);
            Toast.makeText(getActivity().getApplicationContext(),"Draft The Poem",Toast.LENGTH_LONG).show();
            DraftsDataBaseHandler db=new DraftsDataBaseHandler(getActivity());
            db.addPoem(new Poems(poemtitle.getText().toString(), "Pujan paudel", poembody.getText().toString(), String.valueOf(0), monthAndDayText, poemcategory, -1, "-1"));
            FragmentManager manager = getActivity().getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.container,new DraftsFragment()).commit();
        }

    }

    public void PublishToFollowers(final PoemsPost poemsPost){
        final List<String>tobeSent=new ArrayList<String>();
        ParseQuery<FollowList> check=FollowList.getQuery();
        check.whereEqualTo("User", ParseUser.getCurrentUser());
        check.findInBackground(new FindCallback<FollowList>() {
            @Override
            public void done(List<FollowList> list, ParseException e) {

                FollowList action = list.get(0);
                final JSONArray present = action.getJSONArray("Followers");
                Log.d("JSON Array", String.valueOf(present));
                for (int i = 0; i < present.length(); i++) {
                    JSONObject userobject = null;
                    try {
                        userobject = present.getJSONObject(i);
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    Log.d("In Follow Loop", String.valueOf(i));
                    try {
                        tobeSent.add(userobject.getString("UserId"));
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                }//All Items are Allocated Now

                String[] userArr = new String[tobeSent.size()];
                userArr = tobeSent.toArray(userArr);


                ParseQuery<ParseUser> subscriptionQuery = ParseUser.getQuery();
                subscriptionQuery.whereContainedIn("objectId", Arrays.asList(userArr));
                subscriptionQuery.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> list, ParseException e) {
                        Log.d("Subscriptin Size", String.valueOf(list.size()));

                        for (ParseUser user : list) {
                            NotificationFactory nf = new NotificationFactory(ParseUser.getCurrentUser(), user, StaticEnums.Notification.FOLLOWPOST.ordinal(), poemsPost, new Date(), -1,poemsPost.getTitle(),poemsPost.getWriter());
                            nf.Parcel();
                        }
                    }
                });

            }
        });


    }
    public void IncreasePoems() throws ParseException {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("points",String.valueOf(1));
        params.put("userid", ParseUser.getCurrentUser().fetchIfNeeded().getObjectId());
        ParseCloud.callFunctionInBackground("increasePoems", params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
            }
        });

    }

}

