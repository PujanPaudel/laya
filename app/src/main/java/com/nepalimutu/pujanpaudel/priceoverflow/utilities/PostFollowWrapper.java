package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.view.View;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by pujan paudel on 9/20/2015.
 */
public class PostFollowWrapper {
    private PoemsPost actualpost;
    private ParseUser follower;
    private View snackview;

    public PostFollowWrapper(PoemsPost post,ParseUser follow,View v){
        this.actualpost=post;
        this.follower=follow;
        this.snackview=v;
    }

    public void Follow(){
        ParseQuery<PoemsPost>currentPost=PoemsPost.getQuery();
        currentPost.whereEqualTo("objectId", actualpost.getObjectId());
        currentPost.findInBackground(new FindCallback<PoemsPost>() {
            @Override
            public void done(List<PoemsPost> list, ParseException e) {

                //Just 1 Item fr 1 User
            if(e==null){
                if(list.size()>0){
                    PoemsPost post=list.get(0);
                    final JSONArray current=post.getJSONArray("FollowersList");
                    JSONObject userObject=new JSONObject();
                    try {
                        userObject.put("UserId",follower.getObjectId());
                        userObject.put("UserName",follower.getUsername());
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }

                    current.put(userObject);
                    post.put("FollowersList",current);
                    post.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                        if(e==null){
                            PostFollowCache postfc=new PostFollowCache(HomeActivity.reference.getApplicationContext());
                            postfc.addPostFollow(actualpost.getObjectId());
                            if(snackview!=null){
                                SnackbarShow.MySnackbarShow("Followed The Post",snackview);

                            }
                        }
                        }
                    });
                }


            }else{
                Toast.makeText(HomeActivity.reference.getApplicationContext(), "Couldn't Follow the Post Try AGain ", Toast.LENGTH_SHORT).show();
            }

            }
        });
    }


}
