package com.nepalimutu.pujanpaudel.priceoverflow;

import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;

/**
 * Created by pujan paudel on 9/14/2015.
 */
public interface PoemDetailDisplay {
    public void PoemDetail(PoemsPost poem,boolean flag,PoemsPost shared);
}
