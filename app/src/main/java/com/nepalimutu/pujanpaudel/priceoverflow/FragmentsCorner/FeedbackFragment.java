package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.Application;
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.FeedBackFactory;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.GuestFeedBackFactory;
import com.parse.ParseUser;

/**
 * Created by pujan paudel on 10/13/2015.
 */
public class FeedbackFragment extends Fragment {
    private FloatingActionButton info,save;
    private EditText feedbacktext;
    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feedback_forum, container, false);
        info=(FloatingActionButton)view.findViewById(R.id.developerinfo);
        save=(FloatingActionButton)view.findViewById(R.id.uploadfeedback);
        feedbacktext=(EditText)view.findViewById(R.id.edt_feedbackbody);
        final LinearLayout forsnack=(LinearLayout)view.findViewById(R.id.linear);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(HomeActivity.reference.getApplicationContext(),"Developer:Pujan Paudel , Designer: Bibek Mishra",Toast.LENGTH_LONG).show();

                Fragment fragment = new DeveloperInfoFragment();
                FragmentManager manager = getActivity().getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.container, fragment)
                        .addToBackStack(null).commit();


            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(feedbacktext.getText().toString().trim().length()==0){
                    feedbacktext.requestFocus();
                    feedbacktext.setError("Oops!! The FeedBack Can't be Empty");
                    return;
                }
                if(!Application.isGuestUser(getActivity().getApplicationContext())){
                    FeedBackFactory fb=new FeedBackFactory(ParseUser.getCurrentUser(),feedbacktext.getText().toString(),forsnack);
                    fb.Parcel();
                }else{
                    GuestFeedBackFactory fb=new GuestFeedBackFactory(feedbacktext.getText().toString(),forsnack);
                    fb.Parcel();
                }


            }
        });

        return view;
    }

}
