package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.IncreasePointsWrapper;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StaticEnums;
import com.parse.FunctionCallback;
import com.parse.ParseACL;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by pujan paudel on 10/3/2015.
 */
public class ShareFragment extends DialogFragment {

private String socialMedia;
    private PoemsPost sourcepoem;
    private ShareFragment reference;
    private RadioGroup shareplace;
    private int sharestatus;//0==Social Media, 1==In -App
    private static String FACEBOOK="FACEBOOK";
    private static String TWITTER="TWITTER";
    public ShareFragment(){
    }
    @SuppressLint("ValidFragment")
    public ShareFragment(String smedia,PoemsPost source){
        this.socialMedia=smedia;
        this.sourcepoem=source;
    }



    @Override
    public void onCreate(Bundle savedinstancestate){
super.onCreate(savedinstancestate);
        reference=this;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       // View rootView = inflater.inflate(R.layout.sharedialog, container,
               // false);
        final Context contextThemeWrapper=new ContextThemeWrapper(getActivity(),R.style.ShareDialogStyle);
        LayoutInflater localinflater=inflater.cloneInContext(contextThemeWrapper);
        View rootView=localinflater.inflate(R.layout.sharedialog,container,false);
        getDialog().setTitle("Where to Share ?");
        shareplace=(RadioGroup)rootView.findViewById(R.id.shareGroup);
        RadioButton rb=(RadioButton)rootView.findViewById(R.id.socialmedia);
        rb.setText(this.socialMedia);
        shareplace.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.socialmedia:
                        sharestatus = 0;
                        break;
                    case R.id.sharing:
                        //Now do those Fucking Tasks here
                        sharestatus = 1;
                        break;
                }
            }
        });

        Button shareButton=(Button)rootView.findViewById(R.id.share);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reference.dismiss();
                if(sharestatus==0){
                    //Facebook Or Twitter :)
                    if(socialMedia==FACEBOOK){
                        FacebookShare();
                    }else if(socialMedia==TWITTER){
                        TwitterShare();
                    }
                }else{
                    inAppShare();
                }


            }
        });
        return rootView;
    }

    public void inAppShare(){
        final PoemsPost newPost=new PoemsPost();
        newPost.setPostType(1); //0 is for Normal Type  //1  is for third party shar
        newPost.setOriginal(sourcepoem);
        newPost.setCategory(sourcepoem.getCategory());
        //NO need for Third party ;) Let it be Null
        newPost.setThirdParty(ParseUser.getCurrentUser());
        ParseACL acl=new ParseACL();
        acl.setPublicReadAccess(true);
        acl.setPublicWriteAccess(true);
        newPost.setACL(acl);
        newPost.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Toast.makeText(HomeActivity.reference.getApplicationContext(), "Shared the Poem", Toast.LENGTH_LONG).show();
                HashMap<String, String> params = new HashMap<String, String>();
                try {
                    params.put("recieverid", sourcepoem.getWriterUser().fetchIfNeeded().getUsername());
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                params.put("notificationcategory", String.valueOf(10));  //Need to Correct this
                Log.d("Calling", "Cloud Function");
                ParseCloud.callFunctionInBackground("ratesharePush", params, new FunctionCallback<String>() {
                    @Override
                    public void done(String s, ParseException e) {
                    }
                });

                try {
                    FragmentManager fManager = HomeActivity.reference.getSupportFragmentManager();
                    Fragment f = fManager.findFragmentById(R.id.container);
                    if(f instanceof CommentFragment){
                        ((CommentFragment)f).ca.SendNotification();
                    }
                    IncreasePoints();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }


    public void FacebookShare(){
        CallbackManager callbackManager=CallbackManager.Factory.create();

        final ShareDialog shareDialog=new ShareDialog(HomeActivity.reference);

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                // TODO Auto-generated method stub
                //Increase the USer's Score by 10
                Log.d("Facebook", "On Succcess");
            }

            @Override
            public void onCancel() {
                // TODO Auto-generated method stub
                Log.d("Facebook", "On Cancel");

                Toast.makeText(HomeActivity.reference.getApplicationContext(), "Oops !! The Sharing Gone Cancelled", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onError(FacebookException error) {
                // TODO Auto-generated method stub
                HomeActivity.reference.facebookshare = false;

                error.printStackTrace();
            }

        });


        if(shareDialog.canShow(ShareLinkContent.class)){

            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(HomeActivity.reference.getApplicationContext());

            try {
                sp.edit().putString("SharerID", sourcepoem.getWriterUser().fetchIfNeeded().getUsername()).commit();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ShareLinkContent linkContent=new ShareLinkContent.Builder().setContentTitle(sourcepoem.getTitle()).setContentDescription(sourcepoem.getFullPoem())
                    .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.nepalimutu.pujanpaudel.priceoverflow")).build();

            shareDialog.show(linkContent);
        }


    }

    public void TwitterShare(){

        Intent intent=null;

        SharedPreferences tp = PreferenceManager.getDefaultSharedPreferences(HomeActivity.reference.getApplicationContext());

        try {
            tp.edit().putString("SharerID", sourcepoem.getWriterUser().fetchIfNeeded().getUsername()).commit();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {

            intent=new TweetComposer.Builder(HomeActivity.reference).text("Join Laya to Read "+sourcepoem.getTitle()).url(new URL("https://play.google.com/store/apps/details?id=com.nepalimutu.pujanpaudel.priceoverflow")).createIntent();
            //The Url Would be my applications URL AFTER WARDS
            intent.putExtra("UserId",sourcepoem.getWriterUser().fetchIfNeeded().getObjectId());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        HomeActivity.reference.startActivityForResult(intent, 101);
    }



    public void IncreasePoints() throws ParseException {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userid",sourcepoem.getWriterUser().fetchIfNeeded().getObjectId());
        params.put("points", String.valueOf(10));//Let This Stuff be 5 for Now
        ParseCloud.callFunctionInBackground("increasePoints", params, new FunctionCallback<String>() {
            @Override
            public void done(String arrays, ParseException e) {
                if (e == null) {
                    //Success Error Not Called Problem
                    Log.d("Status Returned", arrays);
                    Toast.makeText(getActivity().getApplicationContext(), "COOL!! You Shared the Poem ", Toast.LENGTH_LONG).show();
                } else {
                    e.printStackTrace();
                }
            }
        });
        new IncreasePointsWrapper(ParseUser.getCurrentUser(),5).CloudIncrease();
        //Bless 5  points to thyself

    }
}


