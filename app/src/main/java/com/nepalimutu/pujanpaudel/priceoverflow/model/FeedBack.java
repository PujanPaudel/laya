package com.nepalimutu.pujanpaudel.priceoverflow.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by pujan paudel on 10/13/2015.
 */
@ParseClassName("FeedBacks")
public class FeedBack extends ParseObject{


    public void setAuthor(ParseUser user){
        put("feedbacker",user);
    }

    public void  setFeedBack(String feedback){
        put("feedbackbody",feedback);
    }
}
