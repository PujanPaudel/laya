package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.CommentFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.FeedFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.NotificationFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.parse.DeleteCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;

/**
 * Created by pujan paudel on 10/16/2015.
 */
public class ClearCache {

    public static void ClearPoems(){
        ParseObject.unpinAllInBackground(FeedFragment.poemspin, new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null){
                    Toast.makeText(HomeActivity.reference.getApplicationContext(),"Cleared All the Poems",Toast.LENGTH_LONG).show();
                }else{
                    e.printStackTrace();
                }

            }
        });
    }
    public static void ClearComments(){
        ParseObject.unpinAllInBackground(CommentFragment.CommentTag, new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null){
                    Toast.makeText(HomeActivity.reference.getApplicationContext(),"Cleared All the Comments",Toast.LENGTH_LONG).show();

                }else{
e.printStackTrace();
                }

            }
        });
    }

    public static void ClearNotifications(){
        ParseObject.unpinAllInBackground(NotificationFragment.NotificationTag, new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null){
                    Toast.makeText(HomeActivity.reference.getApplicationContext(),"Cleared All the Notifications",Toast.LENGTH_LONG).show();

                }else{
                    e.printStackTrace();
                }
            }
        });
    }
}
