package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.nepalimutu.pujanpaudel.priceoverflow.R;

/**
 * Created by pujan paudel on 10/15/2015.
 */
public class DeveloperInfoFragment extends Fragment {
private DeveloperInfoFragment reference;
    private ImageButton pujanfb,bibekfb,pujantwitter,bibektwitter;
    private static String PUJANFB_URL="https://www.facebook.com/nepalipujanpaudel";
    private static String BIBEKFB_URL="https://www.facebook.com/bbekmishra";
    private static String PUJANTWITTER_URL="https://twitter.com/CodemyRoad";
    private static String BIBEKTWITTER_URL="https://twitter.com/greeneurons";


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        reference=this;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.developer_info, container,false);
        pujanfb=(ImageButton)rootView.findViewById(R.id.pujanfb);
        bibekfb=(ImageButton)rootView.findViewById(R.id.bibekfb);
        pujantwitter=(ImageButton)rootView.findViewById(R.id.pujantwitter);
        bibektwitter=(ImageButton)rootView.findViewById(R.id.bibektwitter);
        pujanfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThrowIntent(PUJANFB_URL);
            }
        });
        bibekfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThrowIntent(BIBEKFB_URL);
            }
        });

        pujantwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThrowIntent(PUJANTWITTER_URL);
            }
        });
        bibektwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThrowIntent(BIBEKTWITTER_URL);
            }
        });
        return rootView;

    }


    public void ThrowIntent(String uri){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(uri));
        startActivity(i);
    }
}
