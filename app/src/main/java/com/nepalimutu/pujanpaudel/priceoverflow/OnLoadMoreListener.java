package com.nepalimutu.pujanpaudel.priceoverflow;

/**
 * Created by pujan paudel on 9/29/2015.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
