package com.nepalimutu.pujanpaudel.priceoverflow.Adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.Application;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.CommentFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.Interfaces.ShareBackCallback;
import com.nepalimutu.pujanpaudel.priceoverflow.NetworkStatus;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Comments;
import com.nepalimutu.pujanpaudel.priceoverflow.model.CommentsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.DeleteObject;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.LikesCache;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.NotificationFactory;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.RedirectToRegister;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.SnackbarShow;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StaticEnums;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StopLoading;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pujan paudel on 8/24/2015.
 */
public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StopLoading,ShareBackCallback{
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER=2;
    private List<CommentsPost> commentsList;
    private PoemsPost poembody;
    private Context ctx;
    private View snackview;
    public boolean contentflag=false;
    private int comments=0;
    NewCommentViewFooter footerholder;

    private int mLastPosition=0;
    public CommentAdapter(List<CommentsPost> contactList,PoemsPost poem,View v) {
        this.commentsList = contactList;
        this.poembody=poem;
        snackview=v;
    }


    @Override
    public int getItemCount() {

        if(contentflag){
           // 0 items
          this.contentflag=false;
          return 2;
        }else{
            return commentsList.size()+2;

 }
    }


    @Override
    public int getItemViewType(int position) {
        Log.d("My position",String.valueOf(position));
if(contentflag){
   if (position==0){
       return TYPE_HEADER;
   }else{
       return TYPE_FOOTER;
   }
}else{
    //This Works Cool for Normal Type
    if(position==0){
        Log.d("View","Header");
        return TYPE_HEADER;
    } else if(position==commentsList.size()+1){
        return TYPE_FOOTER;
    }
    else{
        return TYPE_ITEM;
    }
}


    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder contactViewHolder, int position) {

        if(contactViewHolder instanceof FullPoemViewHeader){
            final FullPoemViewHeader PoemHeader=(FullPoemViewHeader)contactViewHolder;
            PoemHeader.fullpoem.setText(this.poembody.getFullPoem());
            PoemHeader.fullpoem.setText(this.poembody.getFullPoem());
            PoemHeader.postbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())){
                        SnackbarShow.MySnackbarShow("Can't Comment The Poem While Offline",snackview);
                        return;
                    }
                    if(Application.isGuestUser(ctx)){
                        RedirectToRegister.SnackShow(snackview,HomeActivity.reference);
                        return;
                    }
                    CommentFragment.reference.PostComment(PoemHeader.newcomment.getText().toString(), poembody);
                }
            });


            PoemHeader.upvote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final LikesCache likecache=new LikesCache(HomeActivity.reference.getApplicationContext());

                    if(Application.isGuestUser(ctx)){
                        RedirectToRegister.SnackShow(snackview,HomeActivity.reference);
                        return;
                    }
                    if(!NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())){
                        SnackbarShow.MySnackbarShow("Can't Rate The Poem While Offline",snackview);
                            return;
                    }
                    if(likecache.getRating(poembody.getObjectId())){
                        SnackbarShow.MySnackbarShow("You Have Already Rated the Poem",snackview);
                   }else{
                        CommentFragment.reference.showRatings(poembody,snackview);

                    }

                }
            });
            PoemHeader.addcomment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PoemHeader.newcomment.requestFocus();
            PoemHeader.newcomment.requestFocus();
                    InputMethodManager imm = (InputMethodManager)HomeActivity.reference.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(PoemHeader.newcomment, InputMethodManager.SHOW_IMPLICIT);



                }
            });

            PoemHeader.sharePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Application.isGuestUser(ctx)){
                        RedirectToRegister.SnackShow(snackview,HomeActivity.reference);
                        return;
                    }
                    if(!NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())){
                        SnackbarShow.MySnackbarShow("Can't Share The Poem While Offline",snackview);
                        return;
                    }
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
                    switch (sp.getString("Authentication","UnAuthorized")){
                        case "TWITTER":
                        CommentFragment.reference.showShareDialog("TWITTER",poembody);
                            break;
                      case "FACEBOOK":
                            CommentFragment.reference.showShareDialog("FACEBOOK",poembody);
                            break;
                        case "UnAuthorized":
                            Toast.makeText(ctx,"Cannot Share , Authorize First to Share ", Toast.LENGTH_SHORT).show();
                            break;
                    }

                }
       });

        }

        else if(contactViewHolder instanceof NewCommentViewFooter){
            final NewCommentViewFooter CommentFooter=(NewCommentViewFooter)contactViewHolder;
            footerholder=CommentFooter;
            CommentFooter.progressBar.setIndeterminate(true);
            //set All Those Listeners Here

        }
        else if(contactViewHolder instanceof  CommentsViewHolder){
            comments++;
            Log.d("Comments",String.valueOf(comments));
            //CommentsL\ist.Size is just Fine
            Log.d("OnBind",String.valueOf(commentsList.size()));
            float initialTranslation=(mLastPosition<=position?1500f:-150f);
            final CommentsPost ci=commentsList.get(position-1);
            final CommentsViewHolder cvh=(CommentsViewHolder)contactViewHolder;
            int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME|  DateUtils.FORMAT_NO_YEAR;
            long millisecond =ci.getCommentDate().getTime();
            String monthAndDayText = DateUtils.formatDateTime(HomeActivity.reference.getApplicationContext(),millisecond, flags);


            cvh.commentdate.setText(monthAndDayText);
            try {
                if(!NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())){

                    ci.getCommentorObject().fetchFromLocalDatastoreInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObject, ParseException e) {
                            if(e==null){
                                cvh.commentor.setText(parseObject.getString("DisplayName"));

                            }

                        }
                    });

                }else{
                    cvh.commentor.setText(ci.getCommentorObject().fetchIfNeeded().getString("DisplayName"));

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            cvh.commentor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.reference.OpenProfile(ci.getCommentorObject());
                }
            });
            cvh.vComment.setText(ci.getCommentBody());
           // cvh.itemView.setTranslationY(initialTranslation);
            //cvh.itemView.animate().setInterpolator(new DecelerateInterpolator(1.0f)).translationY(0f).setDuration(900l).setListener(null);
            mLastPosition=position;
            try {
                if(NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())){
                    if(ci.getCommentorObject().fetchIfNeeded().getObjectId().equals(ParseUser.getCurrentUser().fetchIfNeeded().getObjectId())) {
                        cvh.deleteComment.setVisibility(View.VISIBLE);
                    }else{
                        cvh.deleteComment.setVisibility(View.INVISIBLE);
                    }
                }else{
                    cvh.deleteComment.setVisibility(View.INVISIBLE);

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
            cvh.deleteComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Showlert(ci);
                    // DeleteObject.DeleteComment(ctx,ci);
                }
            });


        }

    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        ctx = viewGroup.getContext();
        if (i == TYPE_HEADER) {
            Log.d("Type","Header");
            View itemView = LayoutInflater.
                   from(viewGroup.getContext()).
                   inflate(R.layout.header_full_poem, viewGroup, false);
             return new FullPoemViewHeader(itemView);

        } else if (i == TYPE_ITEM) {
            Log.d("Type","Item");

            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.comments_view, viewGroup, false);
            return new CommentsViewHolder(itemView);
        }else if(i==TYPE_FOOTER){
            Log.d("Type","Footer");

            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.footer_add_comment, viewGroup, false);
            return new NewCommentViewFooter(itemView);
        }
    return null;
    }

    @Override
    public void stopLoading() {
        Log.d("Hey","Stop Loading");
        if(footerholder!=null){
            Log.d("Yeah","Indeterminate");
            footerholder.progressBar.setVisibility(View.GONE);
        }else{
            Log.d("Shittie ","Our Tea is til not ready Then ");

        }

    }

    @Override
    public void startLoading() {
        if(footerholder!=null) {
            footerholder.progressBar.setVisibility(View.VISIBLE);
            footerholder.progressBar.setIndeterminate(true);

        }else{
            Log.d("Shittie ","Our Tea is til not ready Then ");

        }
        }

    @Override
    public void SendNotification() {
        NotificationFactory nf= null;
        try {
            nf = new NotificationFactory(ParseUser.getCurrentUser(),poembody.getWriterUser(), StaticEnums.Notification.SHARE.ordinal(),poembody,new Date(),-1,poembody.getTitle(),ParseUser.getCurrentUser().fetchIfNeeded().getString("DisplayName"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        nf.Parcel();
        Toast.makeText(HomeActivity.reference.getApplicationContext(),"Sent the Notification",Toast.LENGTH_LONG).show();

    }

    public static class NewCommentViewFooter extends  RecyclerView.ViewHolder{
        protected ProgressBar progressBar;
         public NewCommentViewFooter(View v){
             super(v);
             progressBar=(ProgressBar)v.findViewById(R.id.loadingcomments);


         }

    }


    public static class FullPoemViewHeader extends RecyclerView.ViewHolder{
        protected TextView fullpoem;
        protected  ImageButton upvote;
        protected ImageButton addcomment;
        protected ImageButton sharePost;
        protected EditText newcomment;
        protected Button postbutton;
        public FullPoemViewHeader(View v){
            super(v);
            fullpoem=(TextView)v.findViewById(R.id.poembody);
            upvote=(ImageButton)v.findViewById(R.id.upvote);
            addcomment=(ImageButton)v.findViewById(R.id.downvote);
            sharePost=(ImageButton)v.findViewById(R.id.sharebutton);
            newcomment=(EditText)v.findViewById(R.id.newCommentText);
            postbutton=(Button)v.findViewById(R.id.postcomment);
        }
    }
    public static class CommentsViewHolder extends RecyclerView.ViewHolder {
//implements View.onClickListener needed

        protected CardView cardView;
        protected com.ms.square.android.expandabletextview.ExpandableTextView vComment;
        protected TextView commentdate;
        protected TextView commentor;
        protected View currentview;
        protected ImageButton deleteComment;
        public Comments currentItem;


        public CommentsViewHolder(View v) {
            super(v);
            currentview=v;
            cardView = (CardView) v.findViewById(R.id.card_view);
            vComment =(com.ms.square.android.expandabletextview.ExpandableTextView)v.findViewById(R.id.expand_text_view);
            commentdate=(TextView)v.findViewById(R.id.commentlocation);
            commentor=(TextView)v.findViewById(R.id.commentor);
            deleteComment=(ImageButton)v.findViewById(R.id.deleteComment);

        }
    }


public void Showlert(final CommentsPost ci ){

    AlertDialog.Builder builder =
            new AlertDialog.Builder(HomeActivity.reference,R.style.AppCompatAlertDialogStyle);

    builder.setTitle("Poem Delete ");
    builder.setMessage("Do You Really Want to Delete the Poem ?");
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            DeleteObject.DeleteComment(ctx,ci);

        }
    });
    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    });
    builder.show();
}
}
