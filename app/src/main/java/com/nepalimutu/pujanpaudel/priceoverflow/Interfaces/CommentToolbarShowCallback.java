package com.nepalimutu.pujanpaudel.priceoverflow.Interfaces;

/**
 * Created by pujan paudel on 11/25/2015.
 */
public interface CommentToolbarShowCallback {
    public void showCommentToolbar();
    public void backToNormalToolbar();


}
