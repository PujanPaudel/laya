package com.nepalimutu.pujanpaudel.priceoverflow;

import android.Manifest;
import android.app.*;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.AvatarSelectActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.model.FollowList;
import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.ParseACL;
import com.parse.ParseCloud;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
/**
 * Created by pujan paudel on 8/28/2015.
 */

public class Login extends AppCompatActivity
        {
    private CallbackManager callbackManager;
    private TwitterLoginButton loginButton;
    private static final int RC_SIGN_IN = 0;
    private LinearLayout loginstart;
    private RelativeLayout logincomponent;
    private ProgressBar sign;
    private boolean hasSetUpInitialization;

    private ProgressDialog signing;

    // Google client to communicate with Google

    private boolean mIntentInProgress;
    private boolean signedInUser;
    private ImageView image;
    private TextView username, emailLabel;
    private LinearLayout profileFrame, signinFrame;
    private String Username, Password, EmailAdress;
    private LoginButton lbutton;  //lbutton ,loginButton
   private Button guestlogin;
    @Override
    protected void onCreate(Bundle savedInstanceState){

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(sp.getBoolean("Registered",false)|| com.nepalimutu.pujanpaudel.priceoverflow.Application.isGuestUser(getApplicationContext())){
            Intent homeIntent = new Intent(Login.this, HomeActivity.class);
            startActivity(homeIntent);

        }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        signing = new ProgressDialog(Login.this);
        signing.setMessage("Signing Yoou In to Laya ");


        loginstart=(LinearLayout)findViewById(R.id.loginstartlayout);
        logincomponent=(RelativeLayout)findViewById(R.id.logincomponent);
        loginstart.setVisibility(View.GONE);
        logincomponent.setVisibility(View.VISIBLE);
        sign=(ProgressBar)findViewById(R.id.loadingbar);
        callbackManager = CallbackManager.Factory.create();

        guestlogin=(Button)findViewById(R.id.guest_login);
        guestlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                com.nepalimutu.pujanpaudel.priceoverflow.Application.setGuestUser(getApplicationContext());
                Intent launch=new Intent(Login.this,HomeActivity.class);
startActivity(launch);
            }
        });
        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls

                loginstart.setVisibility(View.VISIBLE);
                logincomponent.setVisibility(View.GONE);

                //String output = "Status: " +
                 //       "Your login was successful " +
                  //      result.data.getUserName() +
                   //     "\nAuth User ID Received: "+
                    //    result.data.getUserId()+
                     //  "\nAuth Token Received: " +
                      //  result.data.getAuthToken().token;
                //Log.d("Id",String.valueOf(result.data.getUserId()));
                //Toast.makeText(getApplicationContext(),output,Toast.LENGTH_LONG).show();

                //Twitter Login, So share only works with Twitter
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                sp.edit().putString("Authentication", "TWITTER").commit();
                ParseSignUp(String.valueOf(result.data.getUserId()));
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
            }
        });






        lbutton=(LoginButton)findViewById(R.id.fb_login_button);
        image = (ImageView) findViewById(R.id.image);
        username = (TextView) findViewById(R.id.username);
        emailLabel = (TextView) findViewById(R.id.email);

        profileFrame = (LinearLayout) findViewById(R.id.profileFrame);
        signinFrame = (LinearLayout) findViewById(R.id.signinFrame);

        lbutton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
            Toast.makeText(getApplicationContext(),loginResult.getAccessToken().getUserId(),Toast.LENGTH_LONG).show();
                loginstart.setVisibility(View.VISIBLE);
                logincomponent.setVisibility(View.GONE);
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                sp.edit().putString("Authentication","FACEBOOK").commit();
                ParseSignUp(loginResult.getAccessToken().getUserId());
                /**
                 *
                 */
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("Canceled","Cancel");
                Toast.makeText(getApplicationContext(),"Cancelled",Toast.LENGTH_LONG).show();

            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                exception.printStackTrace();
            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        callbackManager.onActivityResult(requestCode, responseCode, intent);
        loginButton.onActivityResult(requestCode,responseCode,intent);
    }



            public void ParseSignUp(final String username){
               // signing.show();
//lbutton ,loginButton
                ParseUser user = new ParseUser();
                Username=username;
                user.setUsername(username);
                user.setPassword("LayaApp");
                user.put("AvatarIndex", 5);
                user.put("DisplayName", "Anonymous");
                user.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        // TODO Auto-generated method stub
                        // dialog.dismiss();
                        if (e != null) {
                            // Show the error message
                            // This is Where it Says USername Pujan Paudel Already Taken
                            Toast.makeText(getApplicationContext(), e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            Log.d("Error", String.valueOf(e.getCode()));
                            signin(username);
                        } else {
                            // And This One is Where
                            Toast.makeText(getApplicationContext(), "You Signed Up!!",
                                    Toast.LENGTH_LONG);
                            // Signed Up Should Have Automatically Save the Radius Dude
                            Log.d("Success", "Signed  Up");
                            setFollowersList();
                            setAchievements();
                            SetPreferences();
                            SetPushInstallation();
                            signing.dismiss();
                            //Maybe Launch an EditInfo ACtivity Then the AvatarIntent !! HEHEHEHE

                            Intent avatarIntent = new Intent(Login.this, AvatarSelectActivity.class);
                            startActivity(avatarIntent);
                            // Show the Dialog Now
                        }

                    }

                });
            }

            public void signin(String username){

                //lbutton.setVisibility(View.INVISIBLE);
                //loginButton.setVisibility(View.INVISIBLE);
                        Username=username;


                String password="LayaApp";
                ParseUser.logInInBackground(username, password, new LogInCallback() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {

                        if (e == null) {
                            SetPreferences();
                            SetPushInstallation();
                            signing.dismiss();
                            Intent avatarIntent = new Intent(Login.this, AvatarSelectActivity.class);
                            startActivity(avatarIntent);
                        } else {
                            e.printStackTrace();
                        }
                    }
                });
            }



            public void SetPreferences(){
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    sp.edit().putBoolean("Registered", true).commit();
                    sp.edit().putBoolean("prefSendNotifications",true).commit();

            }

public void setAchievements(){


    //Assuming that our User  has been signed up
    //And can  be accessed using the ParseUser.getCurrentUseR()
    ParseUser.getCurrentUser().put("Rating",2);// Rating shall be 2 for any User
    ParseUser.getCurrentUser().put("Points",30);
    ParseUser.getCurrentUser().put("Poems", 0);
    ParseUser.getCurrentUser().put("bestauthor", " ");
    ParseUser.getCurrentUser().put("bestquote"," ");
    ParseUser.getCurrentUser().put("bestbooks"," ");
    ParseUser.getCurrentUser().put("Interested", " ");
    ParseUser.getCurrentUser().saveInBackground();

}
            public void setFollowersList(){

                //SetFollowersList Only For Signup Not for the Signin
                //Already Set up an One for Them , Retrieve the Previous USers feature in bonus

                FollowList newfollow=new FollowList();
                newfollow.setUser(ParseUser.getCurrentUser());
                newfollow.setArray(new JSONArray());
                ParseACL followACL=new ParseACL();
                followACL.setPublicWriteAccess(true);
                followACL.setPublicReadAccess(true);
                newfollow.setACL(followACL);
                newfollow.saveInBackground();
            }

            public void SetPushInstallation(){
                Map<String,String> params=new HashMap<String,String>();
                params.put("userId", Username);
                ParseCloud.callFunctionInBackground("checkDuplicate", params, new FunctionCallback<String>() {
                    @Override
                    public void done(String s, ParseException e) {

                        Log.d("Response", s);
                        ParseInstallation pi = ParseInstallation.getCurrentInstallation();
                        pi.put("device_id", Username);
                        pi.saveEventually(new SaveCallback() {

                            @Override
                            public void done(ParseException e) {
                                // TODO Auto-generated method stub
                                if (e == null) {
                                    Log.d("PUSH SERVICE",
                                            "OKAY, SomeHow We Registered to The Push Service");
                                } else {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });




                PushService.setDefaultPushCallback(this, HomeActivity.class);
                Context ctx = this.getApplicationContext();
                PushService.subscribe(ctx, "Simple", HomeActivity.class);


            }

            @Override
            public void onBackPressed(){
                Intent a = new Intent(Intent.ACTION_MAIN);
                a.addCategory(Intent.CATEGORY_HOME);
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        }
