package com.nepalimutu.pujanpaudel.priceoverflow.Interfaces;

/**
 * Created by pujan paudel on 10/2/2015.
 */
public interface CommentDeletedCallBack {
    public void NotifyCommentSetChanged();
    public void NotifyPostDeleted();
}
