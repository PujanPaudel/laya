package com.nepalimutu.pujanpaudel.priceoverflow.Adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.Image;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.DraftsFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.FavouritesList;
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Poems;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.DraftsDataBaseHandler;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StaticEnums;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by pujan paudel on 9/13/2015.
 */
public class DraftsAdapter extends RecyclerView.Adapter<DraftsAdapter.DraftsViewHolder> {
    private List<Poems> poemsList;
    private Context ctx;
    private int mLastPosition=0;
    public DraftsAdapter(List<Poems> contactList) {
        this.poemsList = contactList;
    }



    @Override
    public DraftsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ctx=parent.getContext();
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.draft_cardview, parent, false);
        return new DraftsViewHolder(itemView);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public void onBindViewHolder(DraftsViewHolder holder, int position) {
        holder.currentItem=poemsList.get(position);
        float initialTranslation=(mLastPosition<=position?1500f:-150f);

        final Poems ci = poemsList.get(position);

        switch (ci.poemcategory){
            case "Poem":
                holder.poemtype.setImageResource(StaticEnums.poemtypeURL[0]);
                break;

            case "Haiku":
                holder.poemtype.setImageResource(StaticEnums.poemtypeURL[1]);

                break;

            case "Article":
                holder.poemtype.setImageResource(StaticEnums.poemtypeURL[2]);

                break;

            case "Gajals":
                holder.poemtype.setImageResource(StaticEnums.poemtypeURL[3]);

                break;

            case "Miscellaneous":
                holder.poemtype.setImageResource(StaticEnums.poemtypeURL[4]);

                break;

            default:
                holder.poemtype.setImageResource(StaticEnums.poemtypeURL[4]);

        }
        holder.editdraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.reference.openDetail(ci.poemtitle,ci.poembody,ci.poemcategory,ci.db_id);
            }
        });
        holder.deletedraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DraftsDataBaseHandler ddbh=new DraftsDataBaseHandler(HomeActivity.reference.getApplicationContext());
                ddbh.DeleteFavourite(ci.db_id);
                Toast.makeText(HomeActivity.reference.getApplicationContext(), "Deleted the Favourite", Toast.LENGTH_SHORT).show();
                Fragment fragment = new DraftsFragment();
                FragmentManager manager = HomeActivity.reference.getSupportFragmentManager();

                manager.beginTransaction().replace(R.id.container, fragment)
                        .addToBackStack(null).commit();
            }
        });
        //Everything is Simple For now
        //contactViewHolder.avatarview.setImageResource();
        holder.vtitle.setText(ci.poemtitle);
        holder.vcategory.setText(ci.poemcategory);



        holder.vdraft_date.setText(ci.posteddate);

        holder.itemView.setTranslationY(initialTranslation);
        holder.itemView.animate().setInterpolator(new DecelerateInterpolator(1.0f)).translationY(0f).setDuration(900l).setListener(null);
        mLastPosition=position;
    }

    @Override
    public int getItemCount() {
        return poemsList.size();
    }

    public static class DraftsViewHolder extends RecyclerView.ViewHolder{

        protected CardView cardView;
        protected TextView vtitle;
        protected TextView vcategory;
        protected TextView  vdraft_date;
        protected ImageButton deletedraft;
        protected ImageButton uploaddraft;
        protected ImageButton editdraft;
        protected Poems currentItem;
        protected View recyclerview;
        protected ImageView poemtype;

        public DraftsViewHolder(View v) {
            super(v);
            recyclerview=v;
            v.setClickable(true);

            cardView = (CardView) v.findViewById(R.id.draft_card_view);
            vtitle =(TextView)v.findViewById(R.id.draft_title);
            vcategory=(TextView)v.findViewById(R.id.draft_category);
            vdraft_date=(TextView)v.findViewById(R.id.draft_date);
            deletedraft=(ImageButton)v.findViewById(R.id.deletedraft);
            editdraft=(ImageButton)v.findViewById(R.id.edit_draft);
            poemtype=(ImageView)v.findViewById(R.id.draftType);

        }
    }
}
