package com.nepalimutu.pujanpaudel.priceoverflow;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.CommentFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.DeveloperInfoFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.DraftsFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.EditInfoFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.FavouritesFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.FavouritesList;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.FeedFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.FeedbackFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.FeedsContainer;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.LoginFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.NewPostFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.NotificationFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.PoemsListFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.ProfileInfoFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.ProfilePoemsFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.SettingsActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.SignUpFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner.UserSearchFragment;
import com.nepalimutu.pujanpaudel.priceoverflow.Interfaces.CommentToolbarShowCallback;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.ClearCache;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.DraftDetailCall;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.IncreasePointsWrapper;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.ProfileOpen;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.PushBroadCastReciever;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.RedirectToRegister;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.SmallAvatarIndexes;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class HomeActivity extends AppCompatActivity
        implements NavigationDrawerCallbacks,DraftDetailCall ,PoemDetailDisplay,ProfileOpen,CommentToolbarShowCallback{

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    public static HomeActivity reference;
    private int Categorycount=0;
    private int SearchCalled=0;
    public  NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;
    public static  String[] categories;
    private static final int RESULT_SETTINGS = 1;
    private static final int TWEET_COMPOSER=101;
    private boolean commentsflag=false;
    public static boolean facebookshare=false;
    private RelativeLayout container;
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
                FragmentManager fm = getSupportFragmentManager();
        Fragment current=fm.findFragmentById(R.id.container);
        if(current!=null){
            if(current instanceof CommentFragment){
                Log.d("WHOOOOO","I Held CommenTfragment");
                commentsflag=true;
                onNavigationDrawerItemSelected(-1);

            }else if (current instanceof UserSearchFragment) {
                commentsflag=true;
                onNavigationDrawerItemSelected(-1);
            }else if (current instanceof ProfilePoemsFragment){
                commentsflag=true;
                onNavigationDrawerItemSelected(-1);

            }else if(current instanceof FeedsContainer){
                commentsflag=true;
                onNavigationDrawerItemSelected(-1);
            }
                Log.d("I held", current.getClass().getSimpleName());
                //Do the Task That the onNavigationDrawerSelected 3 Does


        }else{
            Log.d("Currently","I Held No Fragment");
        }
        Log.d("OnCreate", "Fired");
        setContentView(R.layout.activity_home);
        reference=this;
        container=(RelativeLayout)findViewById(R.id.snack_parent);


        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mToolbar.inflateMenu(R.menu.home);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ImageButton notification=(ImageButton)mToolbar.findViewById(R.id.toolbar_noti);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
onNavigationDrawerItemSelected(1);            }
        });

        ImageButton refresh=(ImageButton)mToolbar.findViewById(R.id.toolbar_refresh);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNavigationDrawerItemSelected(0);
            }
        });
        this.categories = new String[] {
                "Poem", "Haiku", "Article", "Gajals", "Miscellaneous"
        };
       // s.setOnItemSelectedListener(new CustomOnItemSelectedListener());
       // s.setDropDownWidth(width);
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
               // android.R.layout.simple_spinner_dropdown_item, categories);
       // s.setAdapter(adapter);


        //Now The SearchView Part
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        // populate the navigation drawer

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int avatarsaved = sp.getInt("USER_AVATAR", 8);

if(!Application.isGuestUser(getApplicationContext())){
    try {
        mNavigationDrawerFragment.setUserData(ParseUser.getCurrentUser().fetchIfNeeded().getString("DisplayName"), "", BitmapFactory.decodeResource(getResources(), SmallAvatarIndexes.miniavatars[Integer.parseInt(ParseUser.getCurrentUser().fetchIfNeeded().getNumber("AvatarIndex").toString())]));
    } catch (ParseException e) {
        e.printStackTrace();
    }
}else{
    //Guest User Data
    mNavigationDrawerFragment.setUserData("Guest User","",BitmapFactory.decodeResource(getResources(),SmallAvatarIndexes.miniavatars[0]));
}




        Intent calling=getIntent();
        Bundle extras=calling.getExtras();
        if(extras!=null){
            //String myParam = extras.getString("notificationtype");
            onNavigationDrawerItemSelected(1);
        }


    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;

        if(position==0&&!commentsflag){
           // fragment = new FeedFragment("NoCategory");
            fragment=new FeedsContainer();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.container, fragment)
                    .addToBackStack(null).commit();
        }
        //3:LogOUt
        //4:Sign In
        //5:Sign up

        else if (position==2){
            if(!Application.isGuestUser(getApplicationContext())){
                fragment = new EditInfoFragment();
                FragmentManager manager = getSupportFragmentManager();

                manager.beginTransaction().replace(R.id.container, fragment)
                        .addToBackStack(null).commit();
            }else{
                //Do the Snackbar Show Task
                 RedirectToRegister.SnackShow(container, this);
            }

        }
        else if (position==6){
            fragment = new FeedbackFragment();
            FragmentManager manager = getSupportFragmentManager();

            manager.beginTransaction().replace(R.id.container, fragment)
                    .addToBackStack(null).commit();
        }
        else if(position==1){

            fragment = new NotificationFragment();
            FragmentManager manager = getSupportFragmentManager();

            manager.beginTransaction().replace(R.id.container, fragment)
                    .addToBackStack(null).commit();

        }
        else if(position==7){
            fragment = new DeveloperInfoFragment();//DeveloperInfo
            FragmentManager manager = getSupportFragmentManager();

            manager.beginTransaction().replace(R.id.container, fragment)
                    .addToBackStack(null).commit();

        }
        else if(position==4){
            fragment = new DraftsFragment();
            FragmentManager manager = getSupportFragmentManager();

            manager.beginTransaction().replace(R.id.container, fragment)
                    .addToBackStack(null).commit();
        }else if(position==5){
            fragment = new FavouritesList();
            FragmentManager manager = getSupportFragmentManager();

            manager.beginTransaction().replace(R.id.container, fragment)
                    .addToBackStack(null).commit();
        }
        else if (position==3){
            if(!Application.isGuestUser(getApplicationContext())){
                fragment = new ProfilePoemsFragment(ParseUser.getCurrentUser());// Just  For testing for Thyselves
                FragmentManager manager = getSupportFragmentManager();

                manager.beginTransaction().replace(R.id.container, fragment)
                        .addToBackStack(null).commit();
            }else{
                //Do the Snackbar Show task :D
                RedirectToRegister.SnackShow(container,this);

            }

            }

        }



    @Override
    public void onBackPressed() {
        FragmentManager fManager = getSupportFragmentManager();
        Fragment f = fManager.findFragmentById(R.id.container);
        if (mNavigationDrawerFragment.isDrawerOpen()){
            mNavigationDrawerFragment.closeDrawer();
            return;
        }
        if(f instanceof FeedsContainer){
              Toast.makeText(getApplicationContext(),"Exiting The Application", Toast.LENGTH_LONG).show();
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }
        else if(f instanceof  CommentFragment) {
            FragmentManager manager = getSupportFragmentManager();
            Fragment fragment=new FeedsContainer();

            manager.beginTransaction().replace(R.id.container, fragment).addToBackStack(null)
                    .commit();
        }
         else{


            super.onBackPressed();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
    }
    @Override
    public void onNewIntent(Intent newIntent) {
        this.setIntent(newIntent);
        Log.d("NEW INTENT ", "WOHOOOO");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.home, menu);
            //Retrieve teh SearchMenu and Plug it into the SearchManager
            MenuItem searchItem = menu.findItem(R.id.action_search);

            SearchManager searchManager = (SearchManager) HomeActivity.this.getSystemService(Context.SEARCH_SERVICE);

            SearchView searchView = null;
            if (searchItem != null) {
                searchView = (SearchView) searchItem.getActionView();
            }
            if (searchView != null) {
                searchView.setSearchableInfo(searchManager.getSearchableInfo(HomeActivity.this.getComponentName()));
                searchView.setQueryHint("User Name ");
                final SearchView finalSearchView = searchView;
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        SearchUserBase(query);
                        finalSearchView.clearFocus();
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                });
            }

            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void SearchUserBase(String user) {
        SearchCalled++;
       // if(SearchCalled==1){
            //Proceed
        //}else{
        //    return;
       // }
        final ProgressDialog searchinguser=new ProgressDialog(HomeActivity.this);
        searchinguser.setMessage("Searching The User");
        searchinguser.show();
Log.d("User", user);
        ParseQuery<ParseUser>searchbase=ParseUser.getQuery();
        searchbase.whereStartsWith("DisplayName", user); // Equivalent to LIKE QUERY OF SQL
        searchbase.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> list, ParseException e) {
                if (e == null) {
                    Log.d("Users Size", String.valueOf(list.size()));
                    for (ParseUser searchuser : list) {
                        Log.d("User Found ", searchuser.getUsername());
                    }
                    searchinguser.dismiss();
                    Fragment fragment = new UserSearchFragment(list);
                    FragmentManager manager = getSupportFragmentManager();

                    manager.beginTransaction().replace(R.id.container, fragment)
                            .addToBackStack(null).commit();

                } else {
                    e.printStackTrace();
                }

            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if(!Application.isGuestUser(getApplicationContext())){
                Intent i = new Intent(this, SettingsActivity.class);
                startActivityForResult(i, RESULT_SETTINGS);
                return true;
            }else{
                    //Show the Guest User Dialog :D

            }

        }
        else if(id==R.id.clearcache){
            ClearCache.ClearComments();
            ClearCache.ClearNotifications();
            ClearCache.ClearPoems();
            return true;

        }else if(id==R.id.developed){

            Fragment fragment = new DeveloperInfoFragment();
            FragmentManager manager = getSupportFragmentManager();

            manager.beginTransaction().replace(R.id.container, fragment)
                    .addToBackStack(null).commit();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SETTINGS:
                showUserSettings();
                break;


        }
        switch (resultCode){
            case 0:
                //The USer cancalleed the Sharing Event
                if(requestCode==TWEET_COMPOSER){
                    break;
                }// Similary an else if Check for the FAcebook Share
                else if(requestCode==RESULT_SETTINGS){
                    Toast.makeText(getApplicationContext(),"The Settings were Applied",Toast.LENGTH_LONG).show();
                    break;
                }else{
                    //Faceboook Share , others  are  already handled above
                        return;
                }

                //These are the portions of success
            case -1:
                if(requestCode==TWEET_COMPOSER){
                    //Server Request for the Twitter Thing
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    String objectId=sp.getString("SharerID","Noneself");
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("userid",objectId);
                    params.put("points",String.valueOf(10));//Let This Stuff be 5 for Now
                    FragmentManager fManager = getSupportFragmentManager();
                    Fragment f = fManager.findFragmentById(R.id.container);
                    if(f instanceof CommentFragment){
                        ((CommentFragment)f).ca.SendNotification();
                    }
                    ParseCloud.callFunctionInBackground("increasePoints", params, new FunctionCallback<String>() {
                        @Override
                        public void done(String arrays, ParseException e) {
                            if (e == null) {
                                //Success Error Not Called Problem
                                Log.d("Status Returned", arrays);
                                Toast.makeText(getApplicationContext(), "Cool!! You Shared the Poem ", Toast.LENGTH_LONG).show();
                            } else {
                                e.printStackTrace();
                            }
                        }
                    });
                    new IncreasePointsWrapper(ParseUser.getCurrentUser(),5).CloudIncrease();
                    sendRateSharePush(objectId);
                    break;
                }else{
                    //Facebook Share
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        String objectId=sp.getString("SharerID","Noneself");
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("userid",objectId);
                        params.put("points",String.valueOf(10));//Let This Stuff be 5 for Now
                        FragmentManager fManager = getSupportFragmentManager();
                        Fragment f = fManager.findFragmentById(R.id.container);
                        if(f instanceof CommentFragment){
                            ((CommentFragment)f).ca.SendNotification();
                        }
                        ParseCloud.callFunctionInBackground("increasePoints", params, new FunctionCallback<String>() {
                            @Override
                            public void done(String arrays, ParseException e) {
                                if (e == null) {
                                    //Success Error Not Called Problem
                                    Log.d("Status Returned", arrays);
                                    Toast.makeText(getApplicationContext(), "Cool !! You Shared the Poem ", Toast.LENGTH_LONG).show();
                                } else {
                                    e.printStackTrace();
                                }
                            }
                        });
                    new IncreasePointsWrapper(ParseUser.getCurrentUser(),5).CloudIncrease();
                    sendRateSharePush(objectId);
                    break;
                    }

                }
                //Bundle extras=data.getStriExtras();


        }



    @Override
    public void openDetail(String title, String poembody, String category,int db_id) {

        Fragment fragment = new NewPostFragment(title,poembody,category,db_id);
        FragmentManager manager =getSupportFragmentManager();

        manager.beginTransaction().replace(R.id.container, fragment)
                .addToBackStack(null).commit();


    }

    @Override
    public void PoemDetail(PoemsPost poem,boolean shareflag,PoemsPost shared) {
            Fragment fragment = new CommentFragment(poem,shareflag,shared);
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.container, fragment)
                    .addToBackStack(null).commit();

    }
    @Override
    public void OpenProfile(ParseUser targetuser) {
        Fragment fragment = new ProfilePoemsFragment(targetuser);// Just  For testing for Thyselves
        FragmentManager manager = getSupportFragmentManager();

        manager.beginTransaction().replace(R.id.container, fragment)
                .addToBackStack(null).commit();
    }

    @Override
    public void showCommentToolbar() {
/**
 *         mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
 mToolbar.inflateMenu(R.menu.home);
 setSupportActionBar(mToolbar);
 getSupportActionBar().setDisplayShowTitleEnabled(false);

 */

    }

    @Override
    public void backToNormalToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mToolbar.inflateMenu(R.menu.home);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

    }


    public Toolbar getToolbar(){
        return mToolbar;
    }
    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            Categorycount++;



            if(Categorycount==1){

            }else{
                Toast.makeText(getApplicationContext(),"Loading Poems Category Wise",Toast.LENGTH_LONG).show();
                Fragment fragment = new FeedFragment(parent.getItemAtPosition(pos).toString());
                FragmentManager manager = getSupportFragmentManager();

                manager.beginTransaction().replace(R.id.container, fragment)
                        .addToBackStack(null).commit();
            }


            // NOw We need to Do Fucking Awesome Things HEre
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }

    }

    private void showUserSettings() {
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);

        StringBuilder builder = new StringBuilder();

        builder.append("\n Display Name: "
                + sharedPrefs.getString("prefUsername", "NULL"));

        builder.append("\n Send report:"
                + sharedPrefs.getBoolean("prefSendNotifications", false));
        Log.d("Settings", builder.toString());
        if(NetworkStatus.isNetworkConnected(getApplicationContext())){
            ParseUser.getCurrentUser().put("DisplayName", sharedPrefs.getString("prefUsername", "Anonymous"));
            ParseUser.getCurrentUser().saveInBackground();
            mNavigationDrawerFragment.changeUserName(sharedPrefs.getString("prefUsername", "Anonymous"));
        }else{
            Toast.makeText(getApplicationContext(),"Can't Apply Settings while Offline",Toast.LENGTH_LONG).show();
        }


    }
    public void sendRateSharePush(String userid) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("recieverid", userid);
        params.put("notificationcategory", String.valueOf(10));  //Need to Correct this
        ParseCloud.callFunctionInBackground("ratesharePush", params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
            }
        });

    }

}

