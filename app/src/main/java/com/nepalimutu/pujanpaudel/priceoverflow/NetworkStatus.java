package com.nepalimutu.pujanpaudel.priceoverflow;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by pujan paudel on 9/23/2015.
 */
public class NetworkStatus {

    public static boolean isNetworkConnected(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false; // There are no active networks.
        } else return true;
    }
}
