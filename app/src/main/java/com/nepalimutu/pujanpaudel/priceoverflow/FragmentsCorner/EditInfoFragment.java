package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.PersonalInfoWrapper;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by pujan paudel on 9/18/2015.
 */
public class EditInfoFragment extends Fragment {
    private EditText interested,bestauthor,bestquote,bestbooks;
    private ProgressDialog loadinginfo;
    private EditInfoFragment myself;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        myself=this;
        loadinginfo=new ProgressDialog(getActivity(), AlertDialog.THEME_HOLO_DARK);
        loadinginfo.setMessage("Loading the Info");
        loadinginfo.show();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.personal_info, container, false);

        interested=(EditText)view.findViewById(R.id.edt_interestedin);
        bestauthor=(EditText)view.findViewById(R.id.edt_bestauthor);
        bestquote=(EditText)view.findViewById(R.id.edt_bestquote);
        bestbooks=(EditText)view.findViewById(R.id.edt_bestbooks);


        String bestauthor= null;
        String bestquote=null;
        String bestbook=null;
        String interested=null;
        try {
            bestauthor = ParseUser.getCurrentUser().fetchIfNeeded().getString("bestauthor");
            bestquote=ParseUser.getCurrentUser().fetchIfNeeded().getString("bestquote");
            bestbook =ParseUser.getCurrentUser().fetchIfNeeded().getString("bestbooks");
            interested=ParseUser.getCurrentUser().fetchIfNeeded().getString("Interested");
            this.interested.setText(interested);
            this.bestauthor.setText(bestauthor);
            this.bestquote.setText(bestquote);
            this.bestbooks.setText(bestbook);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        loadinginfo.dismiss();
        final CoordinatorLayout coodlayout=(CoordinatorLayout)view.findViewById(R.id.coordinator);



        FloatingActionButton editinfo=(FloatingActionButton)view.findViewById(R.id.editinfo);


        editinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        new PersonalInfoWrapper(myself.interested.getText().toString(),
                                myself.bestauthor.getText().toString()
                                , myself.bestquote.getText().toString(),
                                myself.bestbooks.getText().toString(),ParseUser.getCurrentUser(),coodlayout).Update();
            }
        });
        return view;
    }
}
