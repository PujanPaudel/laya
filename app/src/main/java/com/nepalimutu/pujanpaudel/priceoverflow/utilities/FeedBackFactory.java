package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.model.FeedBack;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by pujan paudel on 10/13/2015.
 */
public class FeedBackFactory {
    private ParseUser feedbackUser;
    private String feedbackText;
    private View snack;

    private ProgressDialog saving;


    public FeedBackFactory(ParseUser poster,String feedback,View snackview){
        this.feedbackUser=poster;
        this.feedbackText=feedback;
        saving=new ProgressDialog(HomeActivity.reference);
        snack=snackview;
    }

    public void Parcel(){
    saving.setMessage("Posting the Feedback");
        saving.show();

        FeedBack myfeedback=new FeedBack();
        myfeedback.setAuthor(feedbackUser);
        myfeedback.setFeedBack(feedbackText);
        ParseACL acl=new ParseACL();
        acl.setPublicReadAccess(true);
        acl.setPublicWriteAccess(true);
        myfeedback.setACL(acl);
        myfeedback.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                saving.dismiss();
                if(e==null){
                    SnackbarShow.MySnackbarShow("Thanks For Submission",snack);
                }else{
                    SnackbarShow.MySnackbarShow("Sorry !! Some Network Problem",snack);

                }
            }
        });

    }


}
