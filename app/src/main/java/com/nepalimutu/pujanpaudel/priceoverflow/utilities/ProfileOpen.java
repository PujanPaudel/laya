package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import com.parse.ParseUser;

/**
 * Created by pujan paudel on 9/18/2015.
 */
public interface ProfileOpen {
    public void OpenProfile(ParseUser targetuser);
}
