package com.nepalimutu.pujanpaudel.priceoverflow.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by pujan paudel on 10/18/2015.
 */
@ParseClassName("Report")
public class Reports extends ParseObject {

    public void setReporter(ParseUser reporter){
        put("Reporter",reporter);
    }
    public void setReportType(String type){
        put("Type",type);
    }
    public void setExtraText(String detail){
        put("Detail",detail);
    }
    public void setReport(PoemsPost reportpoem){put("Poem",reportpoem);}
}
