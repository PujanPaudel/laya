package com.nepalimutu.pujanpaudel.priceoverflow.Adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.Image;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.Interfaces.ShareBackCallback;
import com.nepalimutu.pujanpaudel.priceoverflow.NetworkStatus;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Poems;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Post;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.FavouriteDataBaseHandler;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StaticEnums;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StopLoading;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.w3c.dom.Text;

/**
 * Created by pujan paudel on 8/12/2015.
 */
public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StopLoading{
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER=2;
    private static final int TYPE_HEADER=0;
    private List<PoemsPost> poemsList;
    private  Context ctx;
    LoadingViewFooter footerholder;
    private int mLastPosition=0;
    public PostAdapter(List<PoemsPost> contactList) {
        this.poemsList = contactList;
    }


    @Override
    public int getItemCount() {
        return poemsList.size()+2;
    }
    @Override
    public int getItemViewType(int position){
        if(position==0){
            return TYPE_HEADER;
        }else if(position==poemsList.size()+1){
            return TYPE_FOOTER;
        }else{
            return TYPE_ITEM;
        }

    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder contactViewHolder, int position) {


        if(contactViewHolder instanceof  ContactViewHolder){
            final ContactViewHolder myHeader=(ContactViewHolder)contactViewHolder;
            myHeader.currentItem=poemsList.get(position-1);
            float initialTranslation=(mLastPosition<=position?1500f:-150f);


            final PoemsPost ci = poemsList.get(position-1);
            //The Adpating aprt goes here



            myHeader.currentview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(myHeader.currentItem.getPostType()==0){
                        HomeActivity.reference.PoemDetail(ci,false,null);
                    }else{
                        HomeActivity.reference.PoemDetail((PoemsPost)ci.getOriginal(),true,ci);
                    }
                }
            });


            myHeader.vfavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FavouriteDataBaseHandler favouriteDataBaseHandler = new FavouriteDataBaseHandler(HomeActivity.reference.getApplicationContext());
                    try {
                        if(myHeader.currentItem.getPostType()==1){
                            int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME|  DateUtils.FORMAT_NO_YEAR;
                            long millisecond=ci.getOriginal().getDate("PostedDate").getTime();
                            String monthAndDayText = DateUtils.formatDateTime(HomeActivity.reference.getApplicationContext(), millisecond, flags);
                            favouriteDataBaseHandler.addPoem(new Poems(ci.getOriginal().getString("Title"),ci.getOriginal().getParseUser("WriterUser").getString("DisplayName"),ci.getOriginal().getString("FullPoem"),String.valueOf(-1),monthAndDayText,ci.getOriginal().getString("Category"),-1,ci.getOriginal().getObjectId()));
                        }else{
                            //ci.getDate
                            int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME|  DateUtils.FORMAT_NO_YEAR;
                            long millisecond=ci.getDate().getTime();
                            String monthAndDayText = DateUtils.formatDateTime(HomeActivity.reference.getApplicationContext(), millisecond, flags);
                            favouriteDataBaseHandler.addPoem(new Poems(ci.getTitle(),ci.getWriterUser().fetchIfNeeded().getString("DisplayName"),ci.getFullPoem(),String.valueOf(-1),monthAndDayText, ci.getCategory(),-1, ci.getObjectId()));

                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            });
            //Everything is Simple For now

            switch (ci.getCategory()){
                case "Poem":
                    myHeader.poemtype.setImageResource(StaticEnums.poemtypeURL[0]);
                    break;

                case "Haiku":
                    myHeader.poemtype.setImageResource(StaticEnums.poemtypeURL[1]);

                    break;

                case "Article":
                    myHeader.poemtype.setImageResource(StaticEnums.poemtypeURL[2]);

                    break;

                case "Gajals":
                    myHeader.poemtype.setImageResource(StaticEnums.poemtypeURL[3]);

                    break;

                case "Miscellaneous":
                    myHeader.poemtype.setImageResource(StaticEnums.poemtypeURL[4]);

                    break;

                default:
                    myHeader.poemtype.setImageResource(StaticEnums.poemtypeURL[4]);

            }

            Log.d("Post Type", String.valueOf(myHeader.currentItem.getPostType()));
            if(myHeader.currentItem.getPostType()==0){
                myHeader.vTitle.setText(ci.getTitle());
                myHeader.vWriter.setText(ci.getWriter());
                myHeader.vSharer.setVisibility(View.GONE);
                myHeader.vPoints.setText(String.valueOf(ci.getPoints()) + " " + "Points");
                myHeader.poempreview.setText(ci.getFullPoem());


            }else if(myHeader.currentItem.getPostType()==1){
                try {
                    myHeader.vSharer.setVisibility(View.VISIBLE);
                    myHeader.vSharer.setText(ci.getThirdParty().fetchIfNeeded().getString("DisplayName") + "  Shared a Poem: ");
                    myHeader.vTitle.setText(ci.getOriginal().fetchIfNeeded().getString("Title"));
                    myHeader.vWriter.setText(ci.getOriginal().fetchIfNeeded().getString("Writer"));
                    myHeader.poempreview.setText(ci.getOriginal().fetchIfNeeded().getString("FullPoem"));
                    myHeader.vSharer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            HomeActivity.reference.OpenProfile(((PoemsPost)ci.getOriginal()).getWriterUser());
                        }
                    });

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    myHeader.vPoints.setText(String.valueOf(ci.getOriginal().fetchIfNeeded().getNumber("Points"))+ "  Points");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }

        else if(contactViewHolder instanceof LoadingViewFooter){
            LoadingViewFooter footer=(LoadingViewFooter)contactViewHolder;
            footerholder=footer;
            footer.progressBar.setIndeterminate(true);

        }

    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ctx=viewGroup.getContext();
        if(i==TYPE_ITEM){
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.postsviewlayout, viewGroup, false);
            return new ContactViewHolder(itemView);
        }else if(i==TYPE_FOOTER){
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.progressloadingfooter, viewGroup, false);
            return new LoadingViewFooter(itemView);
        }else{
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.emptyview, viewGroup, false);
            return new EmptyHeader(itemView);
        }

    }

    @Override
    public void stopLoading() {
        Log.d("Hey", "Stop Loading");
        if(footerholder!=null){
            Log.d("Yeah", "Indeterminate");
            footerholder.progressBar.setVisibility(View.GONE);
        }else{
            Log.d("Shittie ","Our Tea is til not ready Then ");

        }

    }

    @Override
    public void startLoading() {
        if(footerholder!=null) {
            footerholder.progressBar.setVisibility(View.VISIBLE);
            footerholder.progressBar.setIndeterminate(true);

        }else{
            Log.d("Shittie ","Our Tea is til not ready Then ");

        }
    }



    public static class LoadingViewFooter extends  RecyclerView.ViewHolder{
        protected ProgressBar progressBar;
        public LoadingViewFooter(View v){
            super(v);
            progressBar=(ProgressBar)v.findViewById(R.id.otherloading);
        }

    }
    public static class EmptyHeader extends RecyclerView.ViewHolder{
        public EmptyHeader(View v) {
            super(v);
        }
    }
    public static class ContactViewHolder extends RecyclerView.ViewHolder {
//implements View.onClickListener needed

        protected CardView cardView;
        protected TextView vTitle;
        protected TextView vWriter;
        protected TextView vPoints;
        protected TextView vSharer;

        protected ImageButton vfavourite;
        protected ImageView poemtype;
        public PoemsPost currentItem;
        protected View currentview;
        protected TextView poempreview;
        protected TextView more;
        public ContactViewHolder(View v) {
            super(v);
            currentview=v;

            cardView = (CardView) v.findViewById(R.id.card_view);
            vTitle =(TextView)v.findViewById(R.id.title);
            vWriter=(TextView)v.findViewById(R.id.poemwriter);
            vPoints=(TextView)v.findViewById(R.id.points);
            vfavourite=(ImageButton)v.findViewById(R.id.favourite);
            vSharer=(TextView)v.findViewById(R.id.sharer);
            poemtype=(ImageView)v.findViewById(R.id.poemtype);
            poempreview=(TextView)v.findViewById(R.id.poemdesc);
            more=(TextView)v.findViewById(R.id.loadmore);
        }
    }

}