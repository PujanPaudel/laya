package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.nepalimutu.pujanpaudel.priceoverflow.Adapters.FavouritesAdapter;
import com.nepalimutu.pujanpaudel.priceoverflow.DataSetNotifyCallback;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.FavouriteDataBaseHandler;

import java.net.MalformedURLException;

/**
 * Created by pujan paudel on 9/15/2015.
 */
public class FavouritesList extends Fragment implements DataSetNotifyCallback {

    private RecyclerView recList;
    private  FavouritesAdapter da=null;
    public static FavouritesList reference;
    private RecyclerView yesdatalayout;
    private LinearLayout nodatalayout;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        reference=this;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_feed, container, false);
        recList = (RecyclerView)view.findViewById(R.id.cardList);
        yesdatalayout=recList;
        nodatalayout=(LinearLayout)view.findViewById(R.id.nodataScreen);
        nodatalayout.setVisibility(View.GONE);

        Button refreshbutton=(Button)view.findViewById(R.id.refresh);
        refreshbutton.setVisibility(View.GONE);
        FloatingActionButton addpost=(FloatingActionButton)view.findViewById(R.id.addPost);
       addpost.setVisibility(View.INVISIBLE);
        recList.setHasFixedSize(false);
        nodatalayout.setVisibility(View.INVISIBLE);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        FavouriteDataBaseHandler db=new FavouriteDataBaseHandler(getActivity());

        try {
            da = new FavouritesAdapter(db.getAllFeeds());
            if(da.getItemCount()==0){
                nodatalayout.setVisibility(View.VISIBLE);
                yesdatalayout.setVisibility(View.GONE);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        recList.setAdapter(da);

        return view;
    }

    @Override
    public void Notify() {

            da.notifyDataSetChanged();

    }
}
