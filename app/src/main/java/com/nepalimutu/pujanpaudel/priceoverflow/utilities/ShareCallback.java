package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;

/**
 * Created by pujan paudel on 9/18/2015.
 */
public interface ShareCallback {
    public void PoemShared(PoemsPost poem);
}
