package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Poems;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;

/**
 * Created by pujan paudel on 9/15/2015.
 */
public class FavouritesFragment extends Fragment {
    private Poems currentofflinepoem;
    private RecyclerView yesdatalayout;
    private LinearLayout nodatalayout;

    public FavouritesFragment(){

    }
    @SuppressLint("ValidFragment")
    public FavouritesFragment(Poems mypost){
        this.currentofflinepoem=mypost;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.poem_offline_read, container, false);

        TextView title=(TextView)view.findViewById(R.id.poemtitle);
        TextView writtenby=(TextView)view.findViewById(R.id.poemwriter);
        TextView category=(TextView)view.findViewById(R.id.poemcategory);
        TextView postdate=(TextView)view.findViewById(R.id.date);
        TextView fullpoem=(TextView)view.findViewById(R.id.fullpoem);

        title.setText(currentofflinepoem.poemtitle);
        writtenby.setText(currentofflinepoem.writtenby);
        category.setText(currentofflinepoem.poemcategory);
        postdate.setText(String.valueOf(currentofflinepoem.posteddate));
        fullpoem.setText(currentofflinepoem.poembody);


        return view;


    }
}
