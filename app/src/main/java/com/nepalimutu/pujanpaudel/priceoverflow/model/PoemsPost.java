package com.nepalimutu.pujanpaudel.priceoverflow.model;

import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ParseObject;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by pujan paudel on 9/14/2015.
 */
@ParseClassName("Poems")
public class PoemsPost extends ParseObject implements Serializable{


    //a Default Constructor is Required
    public PoemsPost(){
super();
    }

    public String getTitle(){
    return getString("Title");
    }

    public  void setTitle(String title){
        put("Title",title);
    }



    public String getWriter(){

        return getString("Writer");
    }

    public  void setWriter(String writer){
put("Writer",writer);
    }

    public String getCategory(){

        return getString("Category");
    }

    public  void setCategory(String category){
        put("Category",category);
    }

    public Date getDate(){

        return getDate("PostedDate");
    }

    public  void setDate(Date posteddate){
        put("PostedDate",posteddate);
    }

    public String getFullPoem(){

        return getString("FullPoem");
    }

    public  void setFullPoem(String fullPoem){

        put("FullPoem",fullPoem);
    }



public ParseUser getWriterUser(){
    return getParseUser("WriterUser");
}
    public void setWriterUser(ParseUser writer){
        put("WriterUser",writer);
    }

    public  Integer getPoints(){
        return getInt("Points");
    }

    public void setPoints(Integer points){
        put("Points",points);
    }

    // The Initialization of the Array


    public void setFollowersArray(){
        put("FollowersList",new JSONArray());
    }

    public static ParseQuery<PoemsPost> getQuery(){
        return ParseQuery.getQuery(PoemsPost.class);
    }

    public void setPostType(Number type){
        put("PostType",type);
    }
    public Number getPostType(){
        return getNumber("PostType");
    }

    public void  setThirdParty(ParseUser user){
        put("ThirdParty",user);

    }
    public ParseUser getThirdParty(){
        return getParseUser("ThirdParty");
    }

public void setOriginal(PoemsPost orgi){
    put("OriginalPoem",orgi);
}
    public ParseObject getOriginal(){
        return getParseObject("OriginalPoem");
    }



}
