package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.BounceInterpolator;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.nepalimutu.pujanpaudel.priceoverflow.Adapters.PostAdapter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;



/**
 * Created by pujan paudel on 8/11/2015.
 */
import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.NetworkStatus;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.model.ContactInfo;
import com.nepalimutu.pujanpaudel.priceoverflow.model.FollowList;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Poems;
import com.nepalimutu.pujanpaudel.priceoverflow.model.PoemsPost;
import com.nepalimutu.pujanpaudel.priceoverflow.model.Post;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.EndlessRecyclerOnScrollListener;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.ReportCache;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.StaticConfiguration;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;



public class FeedFragment extends Fragment {
    public static  String poemspin="Poems";
    public FeedFragment reference;
    public ProgressDialog pdial;
    private RecyclerView recList;
    private String[] spinnercategories;
    private String[] post_type;
    private List<PoemsPost> feedposts=new ArrayList<PoemsPost>();
    List<ContactInfo> types = new ArrayList<ContactInfo>();
    private PostAdapter postAdapter;
    private String category;
    private int currentskip=0;
    private LinearLayoutManager llm;
    private List<PoemsPost>localposts;
    private RecyclerView yesdatalayout;
    private LinearLayout nodatalayout;
    private TextView datatype;
    private Button refreshbutton;
    public FeedFragment(){
        this.category="NoCategory";
    }
    @SuppressLint("ValidFragment")
    public FeedFragment(String cat){
        this.category=cat;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        Log.d("FeedFragment", "OnCreate Fired");
        reference=this;
        super.onCreate(savedInstanceState);
        this.spinnercategories = new String[] {
                "Grocery", "Electronics", "Sports", "General", "Stores"

        };

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        localposts=null;
        Log.d("FeedFragment", "OnCreateView Fired");
        currentskip=0;
        View view = inflater.inflate(R.layout.activity_feed, container, false);
        recList = (RecyclerView)view.findViewById(R.id.cardList);
        yesdatalayout=recList;
        nodatalayout=(LinearLayout)view.findViewById(R.id.nodataScreen);
        datatype=(TextView)view.findViewById(R.id.dataType);
        refreshbutton=(Button)view.findViewById(R.id.refresh);
        FloatingActionButton addpost=(FloatingActionButton)view.findViewById(R.id.addPost);

        addpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewPost();
            }

        });

        nodatalayout.setVisibility(View.INVISIBLE);

        recList.setHasFixedSize(false);


        //OnClickListener For Refresh
        refreshbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(),"Refreshing ",Toast.LENGTH_LONG).show();
                retrieveList(true);
            }
        });
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        postAdapter = new PostAdapter(feedposts);
        recList.setAdapter(postAdapter);
        feedposts.clear();

        ParseQuery<PoemsPost> poemsquery = PoemsPost.getQuery();
        poemsquery.orderByDescending("createdAt");
        poemsquery.include("user");
        poemsquery.setLimit(20);
        if(this.category=="NoCategory"){
            //No Category Restriction
        }else{
            poemsquery.whereContains("Category", this.category);
        }
        poemsquery.whereEqualTo("PostType", 0);
        Log.d("Before", "Finding In background");
        poemsquery.fromPin(poemspin);
        poemsquery.findInBackground(new FindCallback<PoemsPost>() {
            @Override
            public void done(List<PoemsPost> list, ParseException e) {
                if (e == null && list != null) {
                    for (PoemsPost poems : list) {
                        if (poems.getPostType() == 0)
                            feedposts.add(poems);
                    }
                    localposts = new ArrayList<PoemsPost>(feedposts);
                    Log.d("After Local Query", String.valueOf(localposts.size()));
                    postAdapter.notifyDataSetChanged();
                    recList.scrollBy(1, 0);
                    if (NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())) {
                        retrieveList(true);
                    } else {
                        if (list.size() == 0) {
                            nodatalayout.setVisibility(View.VISIBLE);
                            yesdatalayout.setVisibility(View.INVISIBLE);
                            datatype.setText("No Poems Available");
                        }
                    }
                }

                Log.d("After", "Adding All the Stuffs");
                //

            }
        });
        return view;
    }

    private void retrieveList(final boolean clearflag){
        nodatalayout.setVisibility(View.GONE);
        yesdatalayout.setVisibility(View.VISIBLE);
        datatype.setText("No Poems Available");
        ParseQuery<PoemsPost> poemsquery =PoemsPost.getQuery();
        poemsquery.setLimit(4);

        if(!NetworkStatus.isNetworkConnected(getActivity().getApplicationContext())){
            poemsquery.fromPin(poemspin);
        }
        if(this.category=="NoCategory"){
            //No Category Restriction
        }else{
            poemsquery.whereContains("Category",this.category);
        }
        postAdapter.startLoading();
        poemsquery.orderByDescending("createdAt");
        poemsquery.include("user");
        poemsquery.setSkip(currentskip);
        Log.d("Insight Current Skip", String.valueOf(currentskip));
        poemsquery.findInBackground(new FindCallback<PoemsPost>() {
            @Override
            public void done(List<PoemsPost> list, ParseException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "Some Network Problem", Toast.LENGTH_LONG).show();

                    nodatalayout.setVisibility(View.VISIBLE);
                    yesdatalayout.setVisibility(View.INVISIBLE);
                    datatype.setText("No Poems Available");
                } else {
                    if (clearflag) {
                        feedposts.clear();
                    }
                    ReportCache rc=new ReportCache(getActivity().getApplicationContext());
                    for (PoemsPost poems : list) {
                            feedposts.add(poems);

                    }
                    if (feedposts.size() == 0) {
                        nodatalayout.setVisibility(View.VISIBLE);
                        yesdatalayout.setVisibility(View.INVISIBLE);
                        datatype.setText("No Poems Available");
                    }
                    postAdapter.stopLoading();
                    postAdapter.notifyDataSetChanged();
                    cleanAndAdd(localposts, feedposts, clearflag);
                    recList.scrollBy(1, 0);
                    Log.d("CHECK", "BEFORE SCROLLING SETUP");
                    if (!NetworkStatus.isNetworkConnected(HomeActivity.reference.getApplicationContext())) {
                        return;
                    }
                    recList.addOnScrollListener(new EndlessRecyclerOnScrollListener(llm) {
                        @Override
                        public void onLoadMore() {
                            currentskip += 5;
                            retrieveList(false);
                            recList.removeOnScrollListener(this);
                        }
                    });

                }
            }
        });
        return;
    }

    public void NewPost(){
        Fragment fragment=null;
        if(this.category.equals("NoCategory")){
            fragment = new NewPostFragment("","","Poem",-1);

        }else{
            fragment = new NewPostFragment("","",this.category,-1);

        }
        FragmentManager manager = getActivity().getSupportFragmentManager();

        manager.beginTransaction().replace(R.id.container, fragment)
                .addToBackStack(null).commit();

    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            Toast.makeText(parent.getContext(),
                    "Category Selected : " + parent.getItemAtPosition(pos).toString(),
                    Toast.LENGTH_SHORT).show();

            // NOw We need to Do Fucking Awesome Things HEre
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }
    }


    public void cleanAndAdd(List<PoemsPost>oldcache,List<PoemsPost>newcache,boolean clearflag) {
            Log.d("Clearing","Old Datum");
            Log.d("Size of Old cache",String.valueOf(oldcache.size()));

            ParseObject.unpinAllInBackground(poemspin,oldcache, new DeleteCallback() {
                @Override
                public void done(ParseException e) {
                    if(e==null){
                        Log.d("Cleared","Old Datum");
                    }
                }
            });

        ParseObject.pinAllInBackground(poemspin, newcache, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("Pinned", "Saved In Background");
                }
            }
        });
    }

}

