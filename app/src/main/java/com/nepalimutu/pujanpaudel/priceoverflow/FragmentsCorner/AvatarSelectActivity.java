package com.nepalimutu.pujanpaudel.priceoverflow.FragmentsCorner;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.priceoverflow.HomeActivity;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.AvatarsURL;
import com.nepalimutu.pujanpaudel.priceoverflow.utilities.RoundedImageView;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by pujan paudel on 9/23/2015.
 */
public class AvatarSelectActivity extends AppCompatActivity{
private int currentlyselected=0;
    private RoundedImageView fullavatar;
    private static String USER_AVATAR_ID="USER_AVATAR";
    private  String displayname;
    private EditText displaynametext;


@Override
public void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.avatar_view);
    fullavatar=(RoundedImageView)findViewById(R.id.roundedprofile);
    displaynametext=(EditText)findViewById(R.id.edt_displayname);
    try {
        displaynametext.setText(ParseUser.getCurrentUser().fetchIfNeeded().getString("DisplayName"));
    } catch (ParseException e) {
        e.printStackTrace();
    }
    final CheckBox anonymous=(CheckBox)findViewById(R.id.anonymous);

    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    int avatarsaved = sp.getInt(USER_AVATAR_ID, 8); //Let 1 Be the Default
    Log.d("Saved Value", String.valueOf(avatarsaved));
    fullavatar.setImageResource(AvatarsURL.URLS[avatarsaved]);
    FloatingActionButton saveAvatar=(FloatingActionButton)findViewById(R.id.changeAvatar);

    saveAvatar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           if(anonymous.isChecked()){
               displayname="Anonymous";
           }else{
               displayname=displaynametext.getText().toString();
               if(displayname.length()<=4){
                   Toast.makeText(getApplicationContext(),"Display Name Can't be Less than 3 Digits",Toast.LENGTH_LONG).show();
                   return;
               }
           }
            SharedPreferences sp = PreferenceManager
                    .getDefaultSharedPreferences(getApplicationContext());
            sp.edit().putInt(USER_AVATAR_ID, currentlyselected).apply();
            sp.edit().putString("prefUsername",displayname).apply();

            ParseUser.getCurrentUser().put("AvatarIndex", currentlyselected);
            ParseUser.getCurrentUser().put("DisplayName",displayname);
            ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {

                @Override
                public void done(ParseException e) {
                    if(e==null){
                        Toast.makeText(getApplicationContext(),"Updated the Avatar",Toast.LENGTH_LONG).show();
                        Intent main=new Intent(AvatarSelectActivity.this, HomeActivity.class);
                        startActivity(main);
                    }
                }
            });



        }
    });
}
    public void AvatarClick(View view) {
        switch(view.getId()){

            case R.id.avatar1:
                currentlyselected=0;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();
                break;

            case R.id.avatar2:
                currentlyselected=1;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;

            case R.id.avatar3:
                currentlyselected=2;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;

            case R.id.avatar4:
                currentlyselected=3;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;

            case R.id.avatar5:
                currentlyselected=4;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;

            case R.id.avatar6:
                currentlyselected=5;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;

            case R.id.avatar7:
                currentlyselected=6;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;

            case R.id.avatar8:
                currentlyselected=7;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;


            case R.id.avatar9:
                currentlyselected=8;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;

            case R.id.avatar10:
                currentlyselected=9;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;

            case R.id.avatar11:
                currentlyselected=10;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;

            case R.id.avatar12:
                currentlyselected=11;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;

            case R.id.avatar13:
                currentlyselected=12;
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();

                break;

            case R.id.avatar14:
                currentlyselected=13;
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);

                break;

            case R.id.avatar15:
                currentlyselected=14;
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);

                break;

            case R.id.avatar16:
                currentlyselected=15;
                Toast.makeText(getApplicationContext(),String.valueOf(currentlyselected),Toast.LENGTH_LONG).show();
                fullavatar.setImageResource(AvatarsURL.URLS[currentlyselected]);

                break;


        }
    }
}
