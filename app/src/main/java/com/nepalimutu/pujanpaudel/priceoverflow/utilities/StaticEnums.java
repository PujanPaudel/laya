package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import com.nepalimutu.pujanpaudel.priceoverflow.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pujan paudel on 9/16/2015.
 */
public class StaticEnums {



    public static enum Notification{
        RATE,
        COMMENT,
        SHARE,
        FOLLOWPOST,
        FOLLOWCOMMENT

    }

    public static enum PostType{
        NORMAL_POST,
        SHARE_POST
    }

    public static String[] PoemType={
        "Poem",
        "Haiku",
        "Article",
        "Gajals",
        "Miscellaneous"
    };

    public static int[]poemtypeURL={
            R.drawable.poemtype_poem,
            R.drawable.poemtype_haiku,
            R.drawable.poemtype_article,
            R.drawable.poemtype_gazal,
            R.drawable.poemtype_misc
    };



}
