package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

/**
 * Created by pujan paudel on 10/29/2015.
 */
public interface UserFollowedCallback {
    public void HideFollowButton();
}
