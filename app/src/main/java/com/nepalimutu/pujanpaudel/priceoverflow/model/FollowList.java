package com.nepalimutu.pujanpaudel.priceoverflow.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;

/**
 * Created by pujan paudel on 9/19/2015.
 */

@ParseClassName("FollowList")
public class FollowList extends ParseObject {
    public ParseUser getUser(){
        return getParseUser("User");
    }

    public void setUser(ParseUser user){
        put("User",user);
    }

    public JSONArray getArray(){
        return getJSONArray("Followers");
    }
    public void setArray(JSONArray myarray){
        put("Followers",myarray);
    }
     public static ParseQuery<FollowList> getQuery(){
        return ParseQuery.getQuery(FollowList.class);
    }
}
