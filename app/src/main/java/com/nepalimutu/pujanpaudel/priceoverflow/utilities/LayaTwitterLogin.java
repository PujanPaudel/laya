package com.nepalimutu.pujanpaudel.priceoverflow.utilities;

import android.content.Context;
import android.util.AttributeSet;

import com.nepalimutu.pujanpaudel.priceoverflow.Application;
import com.nepalimutu.pujanpaudel.priceoverflow.R;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

/**
 * Created by pujan paudel on 10/13/2015.
 */
public class LayaTwitterLogin extends TwitterLoginButton {
    public LayaTwitterLogin(Context context) {
        super(context);
        init();
    }

    public LayaTwitterLogin(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LayaTwitterLogin(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        if (isInEditMode()){
            return;
        }
        setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.twiter), null, null, null);
        setBackgroundResource(R.drawable.twiter);
        setTextSize(20);
        setPadding(30, 0, 10, 0);
        setTextColor(getResources().getColor(R.color.tw__blue_default));
    }
}